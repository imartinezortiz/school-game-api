﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class StudentAchievement : BaseEntity
    {
        [Required]
        [ForeignKey("StudentId")]
        public Guid StudentId { get; set; }
        public virtual Student Student { get; set; }

        [Required]
        [ForeignKey("AchievementId")]
        public Guid AchievementId { get; set; }
        public virtual Achievement Achievement { get; set; }

        [Required]
        public int CurrentValue { get; set; }

    }
}
