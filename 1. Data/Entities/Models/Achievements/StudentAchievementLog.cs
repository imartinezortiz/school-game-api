﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class StudentAchievementLog : BaseEntity
    {
        [Required]
        [ForeignKey("StudentAchievementId")]
        public Guid StudentAchievementId { get; set; }
        public virtual StudentAchievement StudentAchievement { get; set; }

        [Required]
        public int CurrentValue { get; set; }

    }
}
