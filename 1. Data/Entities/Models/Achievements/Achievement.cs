﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class Achievement : BaseEntity
    {
        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Name { get; set; }
        
        [Column(TypeName = "varchar(4000)")]
        public string Description { get; set; }

        [Required]
        [Column(TypeName = "varchar(100)")]
        public string AchievementCheckClass { get; set; }

        [Required]
        public int Order { get; set; }

        [Required]
        public int MaxValue { get; set; }
    }
}
