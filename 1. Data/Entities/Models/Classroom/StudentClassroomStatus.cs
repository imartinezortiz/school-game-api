﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class StudentClassroomStatus : BaseEntity
    {
        [Required]
        public int Code { get; set; }

        [Required]
        [Column(TypeName = "varchar(100)")]
        public virtual string Name { get; set; }

    }
}
