﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class ClassroomTeacher : BaseEntity
    {
        [Required]
        [ForeignKey("ClassroomId")]
        public Guid ClassroomId { get; set; }
        public virtual Classroom Classroom { get; set; }
        [Required]
        [ForeignKey("TeacherId")]
        public Guid TeacherId { get; set; }
        public virtual Teacher Teacher { get; set; }

    }
}
