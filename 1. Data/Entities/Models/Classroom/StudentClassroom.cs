﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class StudentClassroom : BaseEntity
    {
        [Column(TypeName = "varchar(4000)")]
        public string Message { get; set; }

        [Required]
        [ForeignKey("StudentClassroomStatusId")]
        public Guid StudentClassroomStatusId { get; set; }
        public virtual StudentClassroomStatus StudentClassroomStatus { get; set; }

        [ForeignKey("ClassroomId")]
        public virtual Guid ClassroomId { get; set; }
        public virtual Classroom Classroom { get; set; }

        [ForeignKey("StudentId")]
        public virtual Guid StudentId { get; set; }
        public virtual Student Student { get; set; }

    }
}
