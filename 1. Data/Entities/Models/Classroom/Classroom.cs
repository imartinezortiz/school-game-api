﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class Classroom : BaseEntity
    {
        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Name { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string SchoolName { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string SchoolGrade { get; set; }

        [Column(TypeName = "varchar(4000)")]
        public string Description { get; set; }

        [Required]
        [StringLength(15, MinimumLength = 15, ErrorMessage = "Length should be 15")]
        [Column(TypeName = "varchar(15)")]
        public string Code { get; set; }

        public virtual List<StudentClassroom> StudentsClassrooms { get; set; }

        public virtual List<ClassroomTeacher> ClassroomsTeachers { get; set; }

    }
}
