﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class Gameplay : BaseEntity
    {
        [Required]
        [ForeignKey("StudentId")]
        public Guid StudentId { get; set; }
        public virtual Student Student { get; set; }

        [Required]
        [ForeignKey("ObjectiveId")]
        public Guid ObjectiveId { get; set; }
        public virtual Word Objective { get; set; }

        [Required]
        [Column(TypeName = "datetimeoffset")]
        public DateTimeOffset StartDate { get; set; } = DateTimeOffset.Now;

        [Column(TypeName = "datetimeoffset")]
        public DateTimeOffset? EndDate { get; set; }

        [Required]
        [Range(typeof(int), "0", "3")]
        public int Level { get; set; } = 0;

        [Required]
        [Range(typeof(int), "0", "10000000")]
        public int Score { get; set; } = 0;

        [Required]
        public bool IsObjectiveAchieved { get; set; } = false;

        public virtual List<GameplayEvent> GameplayEvents { get; set; }

    }
}
