﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class GameplayEvent : BaseEntity
    {
        [Required]
        [ForeignKey("GameplayId")]
        public Guid GameplayId { get; set; }
        public virtual Gameplay Gameplay { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(max)")]
        public string JsonEvent { get; set; } 
    }
}
