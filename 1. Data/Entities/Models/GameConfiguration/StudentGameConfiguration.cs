﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class StudentGameConfiguration : BaseEntity
    {
        [Required]
        [ForeignKey("StudentId")]
        public Guid StudentId { get; set; }
        public virtual Student Student { get; set; }

        [Required]
        public int Level { get; set; } = 0;

        [Required]
        public int SuggestChangeLevelValue { get; set; } = 0;

        [Required]
        public int HighScore { get; set; } = 0;

        public virtual List<StudentGameConfigurationWordDataset> StudentGameConfigurationWordDatasets { get; set; }
    }
}
