﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class StudentGameConfigurationWordDataset : BaseEntity
    {
        [Required]
        [ForeignKey("WordDatasetId")]
        public Guid WordDatasetId { get; set; }
        public virtual WordDataset WordDataset { get; set; }

        [Required]
        [ForeignKey("StudentGameConfigurationId")]
        public Guid StudentGameConfigurationId { get; set; }
        public StudentGameConfiguration StudentGameConfiguration { get; set; }

    }
}
