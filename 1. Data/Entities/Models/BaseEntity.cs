﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public abstract class BaseEntity
    {
        [Required]
        [Key]
        public Guid Id { get; set; }

        [Required]
        [Column(TypeName = "datetimeoffset")]
        public DateTimeOffset CreatedAt { get; set; }

        [Column(TypeName = "datetimeoffset")]
        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "datetimeoffset")]
        public DateTimeOffset? DeletedAt { get; set; }
    }
}