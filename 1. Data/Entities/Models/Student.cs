﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class Student : BaseEntity
    {
        [Required]
        [ForeignKey("UserId")]
        public Guid UserId { get; set; }
        public virtual User User { get; set; }

        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Name { get; set; }

        public virtual StudentClassroom StudentClassroom { get; set; }
        public virtual List<StudentAchievement> StudentAchievements { get; set; }
        public virtual List<StudentLearnedWord> StudentLearnedWords { get; set; }

    }
}
