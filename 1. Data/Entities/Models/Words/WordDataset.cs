﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class WordDataset : BaseEntity
    {
        [ForeignKey("TeacherId")]
        public Guid? TeacherId { get; set; }
        public virtual Teacher Teacher { get; set; }

        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Name { get; set; }

        public bool IsPublic { get; set; }

        public virtual List<Word> Words { get; set; }
        public virtual List<StudentGameConfigurationWordDataset> StudentGameConfigurationWordDatasets { get; set; }
    }
}
