﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class StudentLearnedWord : BaseEntity
    {
        [Required]
        [ForeignKey("WordId")]
        public Guid WordId { get; set; }
        public virtual Word Word { get; set; }

        [Required]
        [ForeignKey("StudentId")]
        public Guid StudentId { get; set; }
        public virtual Student Student { get; set; }

    }
}
