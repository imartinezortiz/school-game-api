﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolGame.Entities.Models
{
    public class Word : BaseEntity
    {
        [Required]
        [Column(TypeName = "varchar(13)")]
        public string Value { get; set; }

        [Required]
        [Column(TypeName = "varchar(250)")]
        public string ImageUrl { get; set; }

        [Required]
        [ForeignKey("WordDatasetId")]
        public Guid WordDatasetId { get; set; }
        public virtual WordDataset WordDataset { get; set; }

    }
}
