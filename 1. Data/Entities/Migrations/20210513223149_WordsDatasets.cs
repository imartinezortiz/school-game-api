﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class WordsDatasets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StudentGameConfigurations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    StudentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentGameConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentGameConfigurations_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WordsDatasets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    TeacherId = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(type: "varchar(100)", nullable: false),
                    IsPublic = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WordsDatasets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WordsDatasets_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentGameConfigurationsWordDatasets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    WordDatasetId = table.Column<Guid>(nullable: false),
                    StudentGameConfigurationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentGameConfigurationsWordDatasets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentGameConfigurationsWordDatasets_StudentGameConfigurations_StudentGameConfigurationId",
                        column: x => x.StudentGameConfigurationId,
                        principalTable: "StudentGameConfigurations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentGameConfigurationsWordDatasets_WordsDatasets_WordDatasetId",
                        column: x => x.WordDatasetId,
                        principalTable: "WordsDatasets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Words",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    Value = table.Column<string>(type: "varchar(13)", nullable: false),
                    ImageUrl = table.Column<string>(type: "varchar(250)", nullable: false),
                    WordDatasetId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Words", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Words_WordsDatasets_WordDatasetId",
                        column: x => x.WordDatasetId,
                        principalTable: "WordsDatasets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "WordsDatasets",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "IsPublic", "Name", "TeacherId", "UpdatedAt" },
                values: new object[] { new Guid("fed1a943-bb8b-4532-81df-f58521c45145"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, true, "Colores", null, new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)) });

            migrationBuilder.InsertData(
                table: "WordsDatasets",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "IsPublic", "Name", "TeacherId", "UpdatedAt" },
                values: new object[] { new Guid("ff474f22-776a-4e39-833c-f42d886f118d"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, true, "Frutas", null, new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)) });

            migrationBuilder.InsertData(
                table: "Words",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ImageUrl", "UpdatedAt", "Value", "WordDatasetId" },
                values: new object[,]
                {
                    { new Guid("dec09784-521f-4574-b25f-5377e320edab"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, "https://api.arasaac.org/api/pictograms/2888?download=false", new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), "Naranja", new Guid("fed1a943-bb8b-4532-81df-f58521c45145") },
                    { new Guid("5f02b4ec-bc9b-4b3e-a9cd-c59e4a21ba9c"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, "https://api.arasaac.org/api/pictograms/2807?download=false", new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), "Rosa", new Guid("fed1a943-bb8b-4532-81df-f58521c45145") },
                    { new Guid("d22cfda1-67b8-4532-989f-6b38fbacb557"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, "https://api.arasaac.org/api/pictograms/2808?download=false", new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), "Rojo", new Guid("fed1a943-bb8b-4532-81df-f58521c45145") },
                    { new Guid("5477a7f9-a9e7-4298-9b54-00d19b48b2b2"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, "https://api.arasaac.org/api/pictograms/4869?download=false", new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), "Azul", new Guid("fed1a943-bb8b-4532-81df-f58521c45145") },
                    { new Guid("29b7a347-381e-4687-94e4-59783bc901f3"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, "https://api.arasaac.org/api/pictograms/2648?download=false", new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), "Amarillo", new Guid("fed1a943-bb8b-4532-81df-f58521c45145") },
                    { new Guid("d5cbfcdc-dde2-4e05-84f6-e54807a86abc"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, "https://api.arasaac.org/api/pictograms/4887?download=false", new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), "Verde", new Guid("fed1a943-bb8b-4532-81df-f58521c45145") },
                    { new Guid("3a88ba95-6729-413c-b13b-ea523891b257"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, "https://api.arasaac.org/api/pictograms/13644?download=false", new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), "Manzana", new Guid("ff474f22-776a-4e39-833c-f42d886f118d") },
                    { new Guid("cb2947ce-e7c2-4f26-9b8c-1b7f9aaec339"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, "https://api.arasaac.org/api/pictograms/2561?download=false", new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), "Pera", new Guid("ff474f22-776a-4e39-833c-f42d886f118d") },
                    { new Guid("61cf4a31-3b22-44c7-a45d-0e3e4814b20c"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, "https://api.arasaac.org/api/pictograms/2483?download=false", new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), "Naranja", new Guid("ff474f22-776a-4e39-833c-f42d886f118d") },
                    { new Guid("d0b18936-19b6-41d1-af94-9a29397e3305"), new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, "https://api.arasaac.org/api/pictograms/2400?download=false", new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), "Fresa", new Guid("ff474f22-776a-4e39-833c-f42d886f118d") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudentGameConfigurations_StudentId",
                table: "StudentGameConfigurations",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentGameConfigurationsWordDatasets_StudentGameConfigurationId",
                table: "StudentGameConfigurationsWordDatasets",
                column: "StudentGameConfigurationId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentGameConfigurationsWordDatasets_WordDatasetId",
                table: "StudentGameConfigurationsWordDatasets",
                column: "WordDatasetId");

            migrationBuilder.CreateIndex(
                name: "IX_Words_WordDatasetId",
                table: "Words",
                column: "WordDatasetId");

            migrationBuilder.CreateIndex(
                name: "IX_WordsDatasets_TeacherId",
                table: "WordsDatasets",
                column: "TeacherId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentGameConfigurationsWordDatasets");

            migrationBuilder.DropTable(
                name: "Words");

            migrationBuilder.DropTable(
                name: "StudentGameConfigurations");

            migrationBuilder.DropTable(
                name: "WordsDatasets");
        }
    }
}
