﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class FixCurrentValueColumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurentValue",
                table: "StudentAchievements");

            migrationBuilder.AddColumn<int>(
                name: "CurrentValue",
                table: "StudentAchievements",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentValue",
                table: "StudentAchievements");

            migrationBuilder.AddColumn<int>(
                name: "CurentValue",
                table: "StudentAchievements",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
