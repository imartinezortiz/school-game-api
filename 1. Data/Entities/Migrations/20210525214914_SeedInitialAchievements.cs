﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class SeedInitialAchievements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudentAchievement_Achievement_AchievementId",
                table: "StudentAchievement");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentAchievement_Students_StudentId",
                table: "StudentAchievement");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentAchievement",
                table: "StudentAchievement");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Achievement",
                table: "Achievement");

            migrationBuilder.RenameTable(
                name: "StudentAchievement",
                newName: "StudentAchievements");

            migrationBuilder.RenameTable(
                name: "Achievement",
                newName: "Achievements");

            migrationBuilder.RenameIndex(
                name: "IX_StudentAchievement_StudentId",
                table: "StudentAchievements",
                newName: "IX_StudentAchievements_StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_StudentAchievement_AchievementId",
                table: "StudentAchievements",
                newName: "IX_StudentAchievements_AchievementId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentAchievements",
                table: "StudentAchievements",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Achievements",
                table: "Achievements",
                column: "Id");

            migrationBuilder.InsertData(
                table: "Achievements",
                columns: new[] { "Id", "AchievementCheckClass", "CreatedAt", "DeletedAt", "Description", "MaxValue", "Name", "Order", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("3a6fdc36-75ae-48b9-bad0-b7b6937a7ab0"), "AchieveNGameplays", new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, null, 5, "Juega 5 partidas", 100, new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)) },
                    { new Guid("200e9fc6-78f6-4e9a-959c-55f191753b79"), "AchieveNWins", new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, null, 5, "Gana 5 partidas", 200, new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)) },
                    { new Guid("33291da8-aa54-44e4-9995-136e7e680bbd"), "AchieveNLearnedWords", new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, null, 5, "Aprende 5 palabras", 300, new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)) },
                    { new Guid("5ed3b637-72ba-41a9-aa0e-75c5a87b274d"), "AchieveNConsecutiveWins", new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, null, 5, "Gana 5 partidas seguidas", 400, new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)) },
                    { new Guid("8f8c4a9c-9bc8-4925-a4a1-c097919278b0"), "AchieveNGameplays", new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, null, 10, "Juega 10 partidas", 500, new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)) },
                    { new Guid("ac002076-838e-4e11-bb94-761995db5b74"), "AchieveNWins", new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, null, 10, "Gana 10 partidas", 600, new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)) },
                    { new Guid("4c8443eb-bc2f-4e8d-9cfd-c4f89962f5b5"), "AchieveNLearnedWords", new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, null, 10, "Aprende 10 palabras", 700, new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)) },
                    { new Guid("5a68df49-3077-4308-8e1a-11a8af045991"), "AchieveNConsecutiveWins", new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)), null, null, 10, "Gana 10 partidas seguidas", 800, new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)) }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_StudentAchievements_Achievements_AchievementId",
                table: "StudentAchievements",
                column: "AchievementId",
                principalTable: "Achievements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentAchievements_Students_StudentId",
                table: "StudentAchievements",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudentAchievements_Achievements_AchievementId",
                table: "StudentAchievements");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentAchievements_Students_StudentId",
                table: "StudentAchievements");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentAchievements",
                table: "StudentAchievements");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Achievements",
                table: "Achievements");

            migrationBuilder.DeleteData(
                table: "Achievements",
                keyColumn: "Id",
                keyValue: new Guid("200e9fc6-78f6-4e9a-959c-55f191753b79"));

            migrationBuilder.DeleteData(
                table: "Achievements",
                keyColumn: "Id",
                keyValue: new Guid("33291da8-aa54-44e4-9995-136e7e680bbd"));

            migrationBuilder.DeleteData(
                table: "Achievements",
                keyColumn: "Id",
                keyValue: new Guid("3a6fdc36-75ae-48b9-bad0-b7b6937a7ab0"));

            migrationBuilder.DeleteData(
                table: "Achievements",
                keyColumn: "Id",
                keyValue: new Guid("4c8443eb-bc2f-4e8d-9cfd-c4f89962f5b5"));

            migrationBuilder.DeleteData(
                table: "Achievements",
                keyColumn: "Id",
                keyValue: new Guid("5a68df49-3077-4308-8e1a-11a8af045991"));

            migrationBuilder.DeleteData(
                table: "Achievements",
                keyColumn: "Id",
                keyValue: new Guid("5ed3b637-72ba-41a9-aa0e-75c5a87b274d"));

            migrationBuilder.DeleteData(
                table: "Achievements",
                keyColumn: "Id",
                keyValue: new Guid("8f8c4a9c-9bc8-4925-a4a1-c097919278b0"));

            migrationBuilder.DeleteData(
                table: "Achievements",
                keyColumn: "Id",
                keyValue: new Guid("ac002076-838e-4e11-bb94-761995db5b74"));

            migrationBuilder.RenameTable(
                name: "StudentAchievements",
                newName: "StudentAchievement");

            migrationBuilder.RenameTable(
                name: "Achievements",
                newName: "Achievement");

            migrationBuilder.RenameIndex(
                name: "IX_StudentAchievements_StudentId",
                table: "StudentAchievement",
                newName: "IX_StudentAchievement_StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_StudentAchievements_AchievementId",
                table: "StudentAchievement",
                newName: "IX_StudentAchievement_AchievementId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentAchievement",
                table: "StudentAchievement",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Achievement",
                table: "Achievement",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_StudentAchievement_Achievement_AchievementId",
                table: "StudentAchievement",
                column: "AchievementId",
                principalTable: "Achievement",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentAchievement_Students_StudentId",
                table: "StudentAchievement",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
