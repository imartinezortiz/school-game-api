﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class NamesYCambioFormaStudentClassrooms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Students_StudentsClassrooms_StudentClassroomId",
                table: "Students");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentsClassrooms_Classrooms_ClassroomId",
                table: "StudentsClassrooms");

            migrationBuilder.DropIndex(
                name: "IX_Students_StudentClassroomId",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "StudentClassroomId",
                table: "Students");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Teachers",
                type: "varchar(100)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<Guid>(
                name: "ClassroomId",
                table: "StudentsClassrooms",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "StudentId",
                table: "StudentsClassrooms",
                nullable: false,
                defaultValue: null);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Students",
                type: "varchar(100)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_StudentsClassrooms_StudentId",
                table: "StudentsClassrooms",
                column: "StudentId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentsClassrooms_Classrooms_ClassroomId",
                table: "StudentsClassrooms",
                column: "ClassroomId",
                principalTable: "Classrooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentsClassrooms_Students_StudentId",
                table: "StudentsClassrooms",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudentsClassrooms_Classrooms_ClassroomId",
                table: "StudentsClassrooms");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentsClassrooms_Students_StudentId",
                table: "StudentsClassrooms");

            migrationBuilder.DropIndex(
                name: "IX_StudentsClassrooms_StudentId",
                table: "StudentsClassrooms");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Teachers");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "StudentsClassrooms");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Students");

            migrationBuilder.AlterColumn<Guid>(
                name: "ClassroomId",
                table: "StudentsClassrooms",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "StudentClassroomId",
                table: "Students",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Students_StudentClassroomId",
                table: "Students",
                column: "StudentClassroomId");

            migrationBuilder.AddForeignKey(
                name: "FK_Students_StudentsClassrooms_StudentClassroomId",
                table: "Students",
                column: "StudentClassroomId",
                principalTable: "StudentsClassrooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentsClassrooms_Classrooms_ClassroomId",
                table: "StudentsClassrooms",
                column: "ClassroomId",
                principalTable: "Classrooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
