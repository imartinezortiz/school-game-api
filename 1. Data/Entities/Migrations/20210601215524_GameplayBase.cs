﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class GameplayBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Gameplays",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    StudentId = table.Column<Guid>(nullable: false),
                    ObjectiveId = table.Column<Guid>(nullable: false),
                    StartDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    EndDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    Level = table.Column<int>(nullable: false),
                    Score = table.Column<int>(nullable: false),
                    IsObjectiveAchieved = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gameplays", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Gameplays_Words_ObjectiveId",
                        column: x => x.ObjectiveId,
                        principalTable: "Words",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Gameplays_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GameplayEvents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    GameplayId = table.Column<Guid>(nullable: false),
                    JsonEvent = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameplayEvents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GameplayEvents_Gameplays_GameplayId",
                        column: x => x.GameplayId,
                        principalTable: "Gameplays",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GameplayEvents_GameplayId",
                table: "GameplayEvents",
                column: "GameplayId");

            migrationBuilder.CreateIndex(
                name: "IX_Gameplays_ObjectiveId",
                table: "Gameplays",
                column: "ObjectiveId");

            migrationBuilder.CreateIndex(
                name: "IX_Gameplays_StudentId",
                table: "Gameplays",
                column: "StudentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameplayEvents");

            migrationBuilder.DropTable(
                name: "Gameplays");
        }
    }
}
