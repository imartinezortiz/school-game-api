﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class ClassroomsBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassroomTeacher_Classrooms_ClassroomId",
                table: "ClassroomTeacher");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassroomTeacher_Teachers_TeacherId",
                table: "ClassroomTeacher");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassroomTeacher",
                table: "ClassroomTeacher");

            migrationBuilder.RenameTable(
                name: "ClassroomTeacher",
                newName: "ClassroomsTeachers");

            migrationBuilder.RenameIndex(
                name: "IX_ClassroomTeacher_TeacherId",
                table: "ClassroomsTeachers",
                newName: "IX_ClassroomsTeachers_TeacherId");

            migrationBuilder.RenameIndex(
                name: "IX_ClassroomTeacher_ClassroomId",
                table: "ClassroomsTeachers",
                newName: "IX_ClassroomsTeachers_ClassroomId");

            migrationBuilder.AddColumn<Guid>(
                name: "StudentClassroomId",
                table: "Students",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Classrooms",
                type: "varchar(100)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(100)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Classrooms",
                type: "varchar(15)",
                maxLength: 15,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Classrooms",
                type: "varchar(4000)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SchoolGrade",
                table: "Classrooms",
                type: "varchar(100)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SchoolName",
                table: "Classrooms",
                type: "varchar(100)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassroomsTeachers",
                table: "ClassroomsTeachers",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "StudentClassroomStatuses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    Code = table.Column<int>(nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentClassroomStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StudentsClassrooms",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    StudentClassroomStatusId = table.Column<Guid>(nullable: false),
                    ClassroomId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentsClassrooms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentsClassrooms_Classrooms_ClassroomId",
                        column: x => x.ClassroomId,
                        principalTable: "Classrooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentsClassrooms_StudentClassroomStatuses_StudentClassroomStatusId",
                        column: x => x.StudentClassroomStatusId,
                        principalTable: "StudentClassroomStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "StudentClassroomStatuses",
                columns: new[] { "Id", "Code", "CreatedAt", "DeletedAt", "Name", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("eba48d97-8a14-4ebb-8a2e-145b0e381f14"), 0, new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(2800), new TimeSpan(0, 2, 0, 0, 0)), null, "Pending", new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 939, DateTimeKind.Unspecified).AddTicks(9100), new TimeSpan(0, 2, 0, 0, 0)) },
                    { new Guid("f8d8e047-21d1-40c8-a2cd-edf904c5a521"), 1, new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(3919), new TimeSpan(0, 2, 0, 0, 0)), null, "Acepted", new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(3870), new TimeSpan(0, 2, 0, 0, 0)) },
                    { new Guid("100db339-20b8-4e83-b59b-d20d28553e6d"), 2, new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(3973), new TimeSpan(0, 2, 0, 0, 0)), null, "Rejected", new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(3957), new TimeSpan(0, 2, 0, 0, 0)) },
                    { new Guid("5ee654a3-6a54-4281-b84c-9c112d5876c2"), 3, new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(4006), new TimeSpan(0, 2, 0, 0, 0)), null, "Deleted", new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(3993), new TimeSpan(0, 2, 0, 0, 0)) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Students_StudentClassroomId",
                table: "Students",
                column: "StudentClassroomId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentsClassrooms_ClassroomId",
                table: "StudentsClassrooms",
                column: "ClassroomId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentsClassrooms_StudentClassroomStatusId",
                table: "StudentsClassrooms",
                column: "StudentClassroomStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassroomsTeachers_Classrooms_ClassroomId",
                table: "ClassroomsTeachers",
                column: "ClassroomId",
                principalTable: "Classrooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassroomsTeachers_Teachers_TeacherId",
                table: "ClassroomsTeachers",
                column: "TeacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Students_StudentsClassrooms_StudentClassroomId",
                table: "Students",
                column: "StudentClassroomId",
                principalTable: "StudentsClassrooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassroomsTeachers_Classrooms_ClassroomId",
                table: "ClassroomsTeachers");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassroomsTeachers_Teachers_TeacherId",
                table: "ClassroomsTeachers");

            migrationBuilder.DropForeignKey(
                name: "FK_Students_StudentsClassrooms_StudentClassroomId",
                table: "Students");

            migrationBuilder.DropTable(
                name: "StudentsClassrooms");

            migrationBuilder.DropTable(
                name: "StudentClassroomStatuses");

            migrationBuilder.DropIndex(
                name: "IX_Students_StudentClassroomId",
                table: "Students");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassroomsTeachers",
                table: "ClassroomsTeachers");

            migrationBuilder.DropColumn(
                name: "StudentClassroomId",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Classrooms");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Classrooms");

            migrationBuilder.DropColumn(
                name: "SchoolGrade",
                table: "Classrooms");

            migrationBuilder.DropColumn(
                name: "SchoolName",
                table: "Classrooms");

            migrationBuilder.RenameTable(
                name: "ClassroomsTeachers",
                newName: "ClassroomTeacher");

            migrationBuilder.RenameIndex(
                name: "IX_ClassroomsTeachers_TeacherId",
                table: "ClassroomTeacher",
                newName: "IX_ClassroomTeacher_TeacherId");

            migrationBuilder.RenameIndex(
                name: "IX_ClassroomsTeachers_ClassroomId",
                table: "ClassroomTeacher",
                newName: "IX_ClassroomTeacher_ClassroomId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Classrooms",
                type: "varchar(100)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(100)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassroomTeacher",
                table: "ClassroomTeacher",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassroomTeacher_Classrooms_ClassroomId",
                table: "ClassroomTeacher",
                column: "ClassroomId",
                principalTable: "Classrooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassroomTeacher_Teachers_TeacherId",
                table: "ClassroomTeacher",
                column: "TeacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
