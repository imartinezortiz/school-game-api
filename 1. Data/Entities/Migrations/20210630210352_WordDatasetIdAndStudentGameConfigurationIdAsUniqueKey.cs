﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class WordDatasetIdAndStudentGameConfigurationIdAsUniqueKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_StudentGameConfigurationsWordDatasets_WordDatasetId",
                table: "StudentGameConfigurationsWordDatasets");

            migrationBuilder.CreateIndex(
                name: "IX_StudentGameConfigurationsWordDatasets_WordDatasetId_StudentGameConfigurationId",
                table: "StudentGameConfigurationsWordDatasets",
                columns: new[] { "WordDatasetId", "StudentGameConfigurationId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_StudentGameConfigurationsWordDatasets_WordDatasetId_StudentGameConfigurationId",
                table: "StudentGameConfigurationsWordDatasets");

            migrationBuilder.CreateIndex(
                name: "IX_StudentGameConfigurationsWordDatasets_WordDatasetId",
                table: "StudentGameConfigurationsWordDatasets",
                column: "WordDatasetId");
        }
    }
}
