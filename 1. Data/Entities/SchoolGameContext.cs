﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.Extensions.Configuration;
using SchoolGame.Entities.Models;
using System;

namespace SchoolGame.Entities
{
    public class SchoolGameContext : DbContext
    {
        private readonly string connectionString;

        public DbSet<Achievement> Achievements { get; set; }
        public DbSet<Classroom> Classrooms { get; set; }
        public DbSet<ClassroomTeacher> ClassroomsTeachers { get; set; }
        public DbSet<Gameplay> Gameplays { get; set; }
        public DbSet<GameplayEvent> GameplayEvents { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentAchievement> StudentAchievements { get; set; }
        public DbSet<StudentAchievementLog> StudentAchievementLogs { get; set; }
        public DbSet<StudentClassroomStatus> StudentClassroomStatuses { get; set; }
        public DbSet<StudentClassroom> StudentsClassrooms { get; set; }
        public DbSet<StudentGameConfiguration> StudentGameConfigurations { get; set; }
        public DbSet<StudentGameConfigurationWordDataset> StudentGameConfigurationsWordDatasets { get; set; }
        public DbSet<StudentLearnedWord> StudentLearnedWords { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Word> Words { get; set; }
        public DbSet<WordDataset> WordsDatasets { get; set; }

        [DbFunction(Name = "JSON_VALUE", Schema = "dbo")]
        public static string JsonValue(string source, string path) => throw new NotSupportedException();

        /*public SchoolGameContext() : base()
        {
            //"Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ShoolGame;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"
            // this.connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ShoolGame;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            //this.connectionString = "Data Source=192.168.253.2, 1433\\MSSQLLocalDB;Initial Catalog=ShoolGame;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            this.connectionString = "Data Source=sql.bsite.net\\MSSQL2016;User ID=proconrad_SchoolGame;Password=nEwbDgFvU9cDJTFmPjNHZsy8sF2dZ2fT;Initial Catalog=proconrad_SchoolGame;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        }*/

        public SchoolGameContext(IConfiguration configuration) : base()
        {
            this.connectionString = configuration.GetConnectionString("SchoolGame");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                // .UseLazyLoadingProxies()
                .UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDbFunction(() => JsonValue(default(string), default(string)))
                .HasTranslation(args => SqlFunctionExpression.Create("JSON_VALUE", args, typeof(string), null)); ;

            builder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            builder.Entity<Student>()
                .HasIndex(s => s.UserId)
                .IsUnique();

            builder.Entity<Teacher>()
                .HasIndex(t => t.UserId)
                .IsUnique();

            builder.Entity<Classroom>()
                .HasIndex(c => c.Code)
                .IsUnique();

            builder.Entity<StudentClassroom>()
                .HasIndex(sc => sc.StudentId)
                .IsUnique();

            builder.Entity<StudentGameConfiguration>()
                .HasIndex(sgc => sgc.StudentId)
                .IsUnique();

            builder.Entity<StudentGameConfigurationWordDataset>()
                .HasIndex(sgcwd => new { sgcwd.WordDatasetId, sgcwd.StudentGameConfigurationId}).IsUnique();

            #region MasterData StudentClassroomStatus
            builder.Entity<StudentClassroomStatus>()
                .HasData(
                    new StudentClassroomStatus {
                        Id = Guid.Parse("eba48d97-8a14-4ebb-8a2e-145b0e381f14"),
                        Code = 0,
                        Name = "Pending",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(2800), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 939, DateTimeKind.Unspecified).AddTicks(9100), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new StudentClassroomStatus {
                        Id = Guid.Parse("f8d8e047-21d1-40c8-a2cd-edf904c5a521"),
                        Code = 1,
                        Name = "Acepted",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(3919), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(3870), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new StudentClassroomStatus {
                        Id = Guid.Parse("100db339-20b8-4e83-b59b-d20d28553e6d"),
                        Code = 2,
                        Name = "Rejected",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(3973), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(3957), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new StudentClassroomStatus {
                        Id = Guid.Parse("5ee654a3-6a54-4281-b84c-9c112d5876c2"),
                        Code = 3,
                        Name = "Deleted",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(4006), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 5, 23, 20, 22, 940, DateTimeKind.Unspecified).AddTicks(3993), new TimeSpan(0, 2, 0, 0, 0))
                    }
                );
            #endregion

            #region MasterData ApplicationWordsDatasets
            builder.Entity<WordDataset>()
                .HasData(
                    new WordDataset
                    {
                        Id = Guid.Parse("fed1a943-bb8b-4532-81df-f58521c45145"),
                        IsPublic = true,
                        Name = "Colores",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new WordDataset
                    {
                        Id = Guid.Parse("ff474f22-776a-4e39-833c-f42d886f118d"),
                        IsPublic = true,
                        Name = "Frutas",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    }
                );

            builder.Entity<Word>()
                .HasData(
                    new Word
                    {
                        Id = Guid.Parse("dec09784-521f-4574-b25f-5377e320edab"),
                        WordDatasetId = Guid.Parse("fed1a943-bb8b-4532-81df-f58521c45145"),
                        Value = "Naranja",
                        ImageUrl = "https://api.arasaac.org/api/pictograms/2888?download=false",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Word
                    {
                        Id = Guid.Parse("5f02b4ec-bc9b-4b3e-a9cd-c59e4a21ba9c"),
                        WordDatasetId = Guid.Parse("fed1a943-bb8b-4532-81df-f58521c45145"),
                        Value = "Rosa",
                        ImageUrl = "https://api.arasaac.org/api/pictograms/2807?download=false",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Word
                    {
                        Id = Guid.Parse("d22cfda1-67b8-4532-989f-6b38fbacb557"),
                        WordDatasetId = Guid.Parse("fed1a943-bb8b-4532-81df-f58521c45145"),
                        Value = "Rojo",
                        ImageUrl = "https://api.arasaac.org/api/pictograms/2808?download=false",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Word
                    {
                        Id = Guid.Parse("5477a7f9-a9e7-4298-9b54-00d19b48b2b2"),
                        WordDatasetId = Guid.Parse("fed1a943-bb8b-4532-81df-f58521c45145"),
                        Value = "Azul",
                        ImageUrl = "https://api.arasaac.org/api/pictograms/4869?download=false",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Word
                    {
                        Id = Guid.Parse("29b7a347-381e-4687-94e4-59783bc901f3"),
                        WordDatasetId = Guid.Parse("fed1a943-bb8b-4532-81df-f58521c45145"),
                        Value = "Amarillo",
                        ImageUrl = "https://api.arasaac.org/api/pictograms/2648?download=false",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Word
                    {
                        Id = Guid.Parse("d5cbfcdc-dde2-4e05-84f6-e54807a86abc"),
                        WordDatasetId = Guid.Parse("fed1a943-bb8b-4532-81df-f58521c45145"),
                        Value = "Verde",
                        ImageUrl = "https://api.arasaac.org/api/pictograms/4887?download=false",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Word
                    {
                        Id = Guid.Parse("3a88ba95-6729-413c-b13b-ea523891b257"),
                        Value = "Manzana",
                        WordDatasetId = Guid.Parse("ff474f22-776a-4e39-833c-f42d886f118d"),
                        ImageUrl = "https://api.arasaac.org/api/pictograms/13644?download=false",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Word
                    {
                        Id = Guid.Parse("cb2947ce-e7c2-4f26-9b8c-1b7f9aaec339"),
                        Value = "Pera",
                        WordDatasetId = Guid.Parse("ff474f22-776a-4e39-833c-f42d886f118d"),
                        ImageUrl = "https://api.arasaac.org/api/pictograms/2561?download=false",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Word
                    {
                        Id = Guid.Parse("61cf4a31-3b22-44c7-a45d-0e3e4814b20c"),
                        Value = "Naranja",
                        WordDatasetId = Guid.Parse("ff474f22-776a-4e39-833c-f42d886f118d"),
                        ImageUrl = "https://api.arasaac.org/api/pictograms/2483?download=false",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Word
                    {
                        Id = Guid.Parse("d0b18936-19b6-41d1-af94-9a29397e3305"),
                        Value = "Fresa",
                        WordDatasetId = Guid.Parse("ff474f22-776a-4e39-833c-f42d886f118d"),
                        ImageUrl = "https://api.arasaac.org/api/pictograms/2400?download=false",
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 14, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    }
                );
            #endregion

            #region MasterData Achievements
            builder.Entity<Achievement>()
                .HasData(
                    new Achievement
                    {
                        Id = Guid.Parse("3a6fdc36-75ae-48b9-bad0-b7b6937a7ab0"),
                        Name = "Juega 5 partidas",
                        AchievementCheckClass = "AchieveNGameplays",
                        MaxValue = 5,
                        Order = 100,
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Achievement
                    {
                        Id = Guid.Parse("200e9fc6-78f6-4e9a-959c-55f191753b79"),
                        Name = "Gana 5 partidas",
                        AchievementCheckClass = "AchieveNWins",
                        MaxValue = 5,
                        Order = 200,
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Achievement
                    {
                        Id = Guid.Parse("33291da8-aa54-44e4-9995-136e7e680bbd"),
                        Name = "Aprende 5 palabras",
                        AchievementCheckClass = "AchieveNLearnedWords",
                        MaxValue = 5,
                        Order = 300,
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Achievement
                    {
                        Id = Guid.Parse("5ed3b637-72ba-41a9-aa0e-75c5a87b274d"),
                        Name = "Gana 5 partidas seguidas",
                        AchievementCheckClass = "AchieveNConsecutiveWins",
                        MaxValue = 5,
                        Order = 400,
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Achievement
                    {
                        Id = Guid.Parse("8f8c4a9c-9bc8-4925-a4a1-c097919278b0"),
                        Name = "Juega 10 partidas",
                        AchievementCheckClass = "AchieveNGameplays",
                        MaxValue = 10,
                        Order = 500,
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Achievement
                    {
                        Id = Guid.Parse("ac002076-838e-4e11-bb94-761995db5b74"),
                        Name = "Gana 10 partidas",
                        AchievementCheckClass = "AchieveNWins",
                        MaxValue = 10,
                        Order = 600,
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Achievement
                    {
                        Id = Guid.Parse("4c8443eb-bc2f-4e8d-9cfd-c4f89962f5b5"),
                        Name = "Aprende 10 palabras",
                        AchievementCheckClass = "AchieveNLearnedWords",
                        MaxValue = 10,
                        Order = 700,
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    },
                    new Achievement
                    {
                        Id = Guid.Parse("5a68df49-3077-4308-8e1a-11a8af045991"),
                        Name = "Gana 10 partidas seguidas",
                        AchievementCheckClass = "AchieveNConsecutiveWins",
                        MaxValue = 10,
                        Order = 800,
                        CreatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0)),
                        UpdatedAt = new DateTimeOffset(new DateTime(2021, 5, 25, 0, 1, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 2, 0, 0, 0))
                    }
                );
            #endregion
        }
    }
}
