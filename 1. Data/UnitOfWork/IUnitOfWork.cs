﻿using SchoolGame.Entities.Models;
using SchoolGame.Repository;

namespace SchoolGame.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<User> UserRepository { get; }
        IRepository<Student> StudentRepository { get; }
        IRepository<Teacher> TeacherRepository { get; }
        IRepository<Classroom> ClassroomRepository { get; }
        IRepository<ClassroomTeacher> ClassroomTeachersRepository { get; }
        IRepository<Gameplay> GameplaysRepository { get; }
        IRepository<GameplayEvent> GameplayEventsRepository { get; }
        IRepository<StudentClassroom> StudentClassroomRepository { get; }
        IRepository<StudentClassroomStatus> StudentClassroomStatusRepository { get; }
        IRepository<Word> WordRepository { get; }
        IRepository<WordDataset> WordDatasetRepository { get; }
        IRepository<WordDataset> WordDatasetExtendedRepository { get; }
        
        IRepository<StudentGameConfiguration> StudentGameConfigurationRepository { get; }
        IRepository<StudentGameConfigurationWordDataset> StudentGameConfigurationWordDatasetRepository { get; }
        IRepository<StudentGameConfigurationWordDataset> StudentGameConfigurationWordDatasetExtendedRepository { get; }
        IRepository<StudentAchievement> StudentAchievementRepository { get; }
        IRepository<StudentLearnedWord> StudentLearnedWordRepository { get; }
        IRepository<StudentAchievementLog> StudentAchievementLogRepository { get; }
        IRepository<Achievement> AchievementRepository { get; }

        void SaveChanges();
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
    }
}
