﻿using Microsoft.EntityFrameworkCore.Storage;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using SchoolGame.Repository;
using System;

namespace SchoolGame.UnitOfWork
{
    public class UnitOfWorkImplementation : IUnitOfWork
    {
        private SchoolGameContext context;
        private IDbContextTransaction transaction;

        public IRepository<User> UserRepository { get; }
        public IRepository<Student> StudentRepository { get; }
        public IRepository<Teacher> TeacherRepository { get; }
        public IRepository<Classroom> ClassroomRepository { get; }
        public IRepository<ClassroomTeacher> ClassroomTeachersRepository { get; }
        public IRepository<Gameplay> GameplaysRepository { get; }
        public IRepository<GameplayEvent> GameplayEventsRepository { get; }
        public IRepository<StudentClassroom> StudentClassroomRepository { get; }
        public IRepository<StudentClassroomStatus> StudentClassroomStatusRepository { get; }
        public IRepository<Word> WordRepository { get; }
        public IRepository<WordDataset> WordDatasetRepository { get; }
        public IRepository<WordDataset> WordDatasetExtendedRepository { get; }
        
        public IRepository<StudentGameConfiguration> StudentGameConfigurationRepository { get; }
        public IRepository<StudentGameConfigurationWordDataset> StudentGameConfigurationWordDatasetRepository { get; }
        public IRepository<StudentGameConfigurationWordDataset> StudentGameConfigurationWordDatasetExtendedRepository { get; }
        public IRepository<StudentAchievement> StudentAchievementRepository { get; }
        public IRepository<StudentLearnedWord> StudentLearnedWordRepository { get; }
        public IRepository<StudentAchievementLog> StudentAchievementLogRepository { get; }
        public IRepository<Achievement> AchievementRepository { get; }

        public UnitOfWorkImplementation(SchoolGameContext context)
        {
            this.context = context;
            UserRepository = new UserRepository(context);
            StudentRepository = new StudentRepository(context);
            TeacherRepository = new TeacherRepository(context);
            ClassroomRepository = new ClassroomRepository(context);
            GameplaysRepository = new GenericRepository<Gameplay>(context);
            GameplayEventsRepository = new GenericRepository<GameplayEvent>(context);
            StudentClassroomRepository = new StudentClassroomRepository(context);
            StudentClassroomStatusRepository = new GenericRepository<StudentClassroomStatus>(context);
            ClassroomTeachersRepository = new GenericRepository<ClassroomTeacher>(context);
            WordRepository = new GenericRepository<Word>(context);
            WordDatasetRepository = new GenericRepository<WordDataset>(context);
            WordDatasetExtendedRepository = new WordDatasetExtendedRepository(context);
            StudentGameConfigurationRepository = new StudentGameConfigurationRepository(context);
            StudentGameConfigurationWordDatasetRepository = new GenericRepository<StudentGameConfigurationWordDataset>(context);
            StudentGameConfigurationWordDatasetExtendedRepository = new StudentGameConfigurationWordDatasetExtendedRepository(context);
            StudentAchievementRepository = new StudentAchievementRepository(context);
            StudentAchievementLogRepository = new StudentAchievementLogRepository(context);
            StudentLearnedWordRepository = new GenericRepository<StudentLearnedWord>(context);
            AchievementRepository = new GenericRepository<Achievement>(context);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        public void BeginTransaction()
        {
            if (this.transaction != null)
                throw new Exception("Transaction already opened.");

            this.transaction = context.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            this.transaction.Commit();
            this.transaction = null;
        }

        public void RollbackTransaction()
        {
            this.transaction.Rollback();
            this.transaction = null;
        }
    }
}
