﻿using Microsoft.EntityFrameworkCore;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SchoolGame.Repository
{
    public class GenericRepository<TEntity> : IRepository<TEntity>
        where TEntity : BaseEntity
    {
        protected SchoolGameContext context;

        public GenericRepository(SchoolGameContext context)
        {
            this.context = context;
        }

        public virtual IQueryable<TEntity> GetQueryable()
        {
            return context.Set<TEntity>().AsQueryable();
        }

        public async Task<TEntity> Insert(TEntity entity)
        {
            entity.CreatedAt = DateTimeOffset.Now;
            return (await context.AddAsync(entity))
                .Entity;
        }

        public TEntity Update(TEntity entity)
        {
            entity.UpdatedAt = DateTimeOffset.Now;
            
            /*TEntity entityFromDb = this.Find(x => x.Id.Equals(entity.Id)).FirstOrDefault();
            context.Entry(entityFromDb).CurrentValues.SetValues(entity);*/
            
            return context.Update(entity)
                .Entity;
        }

        public TEntity Get(Guid id)
        {
            return context.Set<TEntity>()
                .Find(id);
        }

        public IEnumerable<TEntity> All()
        {
            return GetQueryable()
                .ToList();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return GetQueryable()
                .AsQueryable()
                .Where(predicate)
                .ToList();
        }

        public async Task<TEntity> GetAsync(Guid id)
        {
            return await context.Set<TEntity>()
                .FindAsync(id);
        }

        public async Task<IEnumerable<TEntity>> AllAsync()
        {
            return await GetQueryable()
                .ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await GetQueryable()
                .AsQueryable()
                .Where(predicate)
                .ToListAsync();
        }

        public void Delete(Guid id)
        {
            TEntity entity = Get(id);
            context.Set<TEntity>()
                .Remove(entity);
        }
        public void Delete(TEntity entity)
        {
            context.Set<TEntity>()
                .Remove(entity);
        }

        public void DeleteAll(Expression<Func<TEntity, bool>> predicate)
        {
            var entities = Find(predicate);
            context.Set<TEntity>()
                .RemoveRange(entities);
        }

        public async Task DeleteAsync(Guid id)
        {
            TEntity entity = await GetAsync(id);
            context.Set<TEntity>()
                .Remove(entity);
        }

        public async Task DeleteAllAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var entities = await FindAsync(predicate);
            context.Set<TEntity>()
                .RemoveRange(entities);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}
