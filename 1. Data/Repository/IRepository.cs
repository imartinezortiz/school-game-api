﻿using SchoolGame.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SchoolGame.Repository
{
    public interface IRepository<TEntity>
        where TEntity: BaseEntity
    {
        IQueryable<TEntity> GetQueryable();
        Task<TEntity> Insert(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Get(Guid id);
        IEnumerable<TEntity> All();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> GetAsync(Guid id);
        Task<IEnumerable<TEntity>> AllAsync();
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);
        void Delete(Guid id);
        void Delete(TEntity entity);
        void DeleteAll(Expression<Func<TEntity, bool>> predicate);
        Task DeleteAsync(Guid id);
        Task DeleteAllAsync(Expression<Func<TEntity, bool>> predicate);
        void SaveChanges();
    }
}
