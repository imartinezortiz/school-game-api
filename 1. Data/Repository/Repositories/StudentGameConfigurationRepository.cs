﻿using Microsoft.EntityFrameworkCore;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using System.Linq;

namespace SchoolGame.Repository
{
    public class StudentGameConfigurationRepository : GenericRepository<StudentGameConfiguration>
    {
        public StudentGameConfigurationRepository(SchoolGameContext context) : base(context)
        {
            
        }

        public override IQueryable<StudentGameConfiguration> GetQueryable()
        {
            return base.GetQueryable()
                .Include(sgc => sgc.Student)
                    .ThenInclude(s => s.StudentLearnedWords)
                        .ThenInclude(slw => slw.Word)
                .Include(sgc => sgc.StudentGameConfigurationWordDatasets)
                    .ThenInclude(sgcwd => sgcwd.WordDataset)
                        .ThenInclude(wd => wd.Words);
        }
    }
}
