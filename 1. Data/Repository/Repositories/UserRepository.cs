﻿using SchoolGame.Entities;
using SchoolGame.Entities.Models;

namespace SchoolGame.Repository
{
    public class UserRepository : GenericRepository<User>
    {
        public UserRepository(SchoolGameContext context) : base(context)
        {
        }
    }
}
