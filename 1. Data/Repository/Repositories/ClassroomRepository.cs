﻿using Microsoft.EntityFrameworkCore;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SchoolGame.Repository
{
    public class ClassroomRepository : GenericRepository<Classroom>
    {
        public ClassroomRepository(SchoolGameContext context) : base(context)
        {
        }

        public override IQueryable<Classroom> GetQueryable()
        {
            return base.GetQueryable()
                .Include(c => c.ClassroomsTeachers);
        }
    }
}
