﻿using Microsoft.EntityFrameworkCore;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using System.Linq;

namespace SchoolGame.Repository
{
    public class TeacherRepository : GenericRepository<Teacher>
    {
        public TeacherRepository(SchoolGameContext context) : base(context)
        {
        }

        public override IQueryable<Teacher> GetQueryable()
        {
            return base.GetQueryable()
                .Include(t => t.User)
                .Include(t => t.ClassroomsTeachers);
        }
    }
}
