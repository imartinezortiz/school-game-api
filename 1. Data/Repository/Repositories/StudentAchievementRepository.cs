﻿using Microsoft.EntityFrameworkCore;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using System;
using System.Linq;

namespace SchoolGame.Repository
{
    public class StudentAchievementRepository : GenericRepository<StudentAchievement>
    {
        public StudentAchievementRepository(SchoolGameContext context) : base(context)
        {
        }

        public override IQueryable<StudentAchievement> GetQueryable()
        {
            return base.GetQueryable()
                .Include(sa => sa.Achievement);
        }

    }
}
