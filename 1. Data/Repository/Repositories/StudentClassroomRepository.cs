﻿using Microsoft.EntityFrameworkCore;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using System;
using System.Linq;

namespace SchoolGame.Repository
{
    public class StudentClassroomRepository : GenericRepository<StudentClassroom>
    {
        public StudentClassroomRepository(SchoolGameContext context) : base(context)
        {
        }

        public override IQueryable<StudentClassroom> GetQueryable()
        {
            return base.GetQueryable()
                .Include(sc => sc.Classroom)
                .Include(sc => sc.StudentClassroomStatus)
                .Include(sc => sc.Student);
        }
    }
}
