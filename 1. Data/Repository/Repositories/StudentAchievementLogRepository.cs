﻿using Microsoft.EntityFrameworkCore;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using System.Linq;

namespace SchoolGame.Repository
{
    public class StudentAchievementLogRepository : GenericRepository<StudentAchievementLog>
    {
        public StudentAchievementLogRepository(SchoolGameContext context) : base(context)
        {
        }

        public override IQueryable<StudentAchievementLog> GetQueryable()
        {
            return base.GetQueryable()
                .Include(sal => sal.StudentAchievement);
        }

    }
}
