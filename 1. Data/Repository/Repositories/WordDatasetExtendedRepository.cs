﻿using Microsoft.EntityFrameworkCore;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using System;
using System.Linq;

namespace SchoolGame.Repository
{
    public class WordDatasetExtendedRepository : GenericRepository<WordDataset>
    {
        public WordDatasetExtendedRepository(SchoolGameContext context) : base(context)
        {
        }

        public override IQueryable<WordDataset> GetQueryable()
        {
            return base.GetQueryable()
                .Include(wd => wd.Words);
        }
    }
}
