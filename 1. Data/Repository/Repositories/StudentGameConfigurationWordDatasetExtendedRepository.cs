﻿using Microsoft.EntityFrameworkCore;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using System.Linq;

namespace SchoolGame.Repository
{
    public class StudentGameConfigurationWordDatasetExtendedRepository : GenericRepository<StudentGameConfigurationWordDataset>
    {
        public StudentGameConfigurationWordDatasetExtendedRepository(SchoolGameContext context) : base(context)
        {
            
        }

        public override IQueryable<StudentGameConfigurationWordDataset> GetQueryable()
        {
            return base.GetQueryable()
                .Include(sgcwd => sgcwd.StudentGameConfiguration)
                .Include(sgcwd => sgcwd.WordDataset);
        }
    }
}
