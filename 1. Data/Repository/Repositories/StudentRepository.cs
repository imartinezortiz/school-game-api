﻿using Microsoft.EntityFrameworkCore;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using System.Linq;

namespace SchoolGame.Repository
{
    public class StudentRepository : GenericRepository<Student>
    {
        public StudentRepository(SchoolGameContext context) : base(context)
        {
        }

        public override IQueryable<Student> GetQueryable()
        {
            return base.GetQueryable()
                .Include(s => s.StudentClassroom)
                .Include(s => s.StudentAchievements)
                    .ThenInclude(sa => sa.Achievement);
        }

    }
}
