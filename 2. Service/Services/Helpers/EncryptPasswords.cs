﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace SchoolGame.Services.Helpers
{
    public static class EncryptPasswords
    {
        public static string GetEncryptedPassword(string passwordToEncrypt, string salt)
        {
            byte[] encryptedPassword = KeyDerivation.Pbkdf2(
                password: passwordToEncrypt,
                salt: Encoding.ASCII.GetBytes(salt),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8);

            return Convert.ToBase64String(encryptedPassword);
        }

        public static string GenerateSalt()
        {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            return Convert.ToBase64String(salt);
        }
    }
}
