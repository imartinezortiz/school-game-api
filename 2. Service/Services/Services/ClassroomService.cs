﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class ClassroomService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly string availableChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static Random random = new Random();

        public ClassroomService(
            IUnitOfWork unitOfWork,
            IMapper mapper
        )
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        
        public async Task<ClassroomSvcModel> GetClassroomByClassroomId(Guid classroomId)
        {
            Classroom classroom = await unitOfWork.ClassroomRepository.GetAsync(classroomId);

            var classroomSvcModel = mapper.Map<ClassroomSvcModel>(classroom);

            return classroomSvcModel;
        }

        public async Task<List<ClassroomSvcModel>> GetClassroomsByTeacherId(Guid teacherId)
        {
            List<Classroom> classrooms = (await unitOfWork.ClassroomRepository.FindAsync(c => c.ClassroomsTeachers.Any(ct => ct.TeacherId.Equals(teacherId)))).ToList();

            var classroomsSvcModel = mapper.Map<List<ClassroomSvcModel>>(classrooms);

            return classroomsSvcModel;
        }

        public async Task<ClassroomSvcModel> CreateNewClassroom(ClassroomSvcModel classroomSvcModel)
        {
            Classroom classroom = mapper.Map<Classroom>(classroomSvcModel);
            classroom.ClassroomsTeachers = classroomSvcModel.TeachersIds.ConvertAll(id => new ClassroomTeacher() { TeacherId = id });
            classroom = await unitOfWork.ClassroomRepository.Insert(classroom);

            unitOfWork.SaveChanges();

            classroomSvcModel = mapper.Map<ClassroomSvcModel>(classroom);

            return classroomSvcModel;
        }

        public async Task<Guid> getClassroomByCode(string code)
        {
            Guid classroomId = (await unitOfWork.ClassroomRepository.FindAsync(c => c.Code.Equals(code)))
                .Select(c => c.Id).FirstOrDefault();

            return classroomId;
        }

        public ClassroomSvcModel UpdateClassroom(ClassroomSvcModel classroomSvcModel)
        {
            Classroom classroom = mapper.Map<Classroom>(classroomSvcModel);
            classroom = unitOfWork.ClassroomRepository.Update(classroom);

            unitOfWork.SaveChanges();

            classroomSvcModel = mapper.Map<ClassroomSvcModel>(classroom);

            return classroomSvcModel;
        }

        public async Task DeleteClassroom(Guid classroomId)
        {
            await unitOfWork.ClassroomTeachersRepository.DeleteAllAsync(ct => ct.ClassroomId.Equals(classroomId));
            await unitOfWork.StudentClassroomRepository.DeleteAllAsync(ct => ct.ClassroomId.Equals(classroomId));
            await unitOfWork.ClassroomRepository.DeleteAsync(classroomId);
            unitOfWork.SaveChanges();
        }

        public string GenerateCode()
        {
            string code = null;
            do
            {
                code = new string(Enumerable.Repeat(availableChars, 15)
                    .Select(s => s[random.Next(s.Length)]).ToArray());

                if (unitOfWork.ClassroomRepository.GetQueryable().Where(c => c.Code.Equals(code)).Any())
                    code = null;
                /*int count = (await unitOfWork.ClassroomRepository.FindAsync(c => c.Code.Equals(code))).Count();
                if (count > 0) code = null;*/
            } while (code == null);
            return code;
        }
    }
}
