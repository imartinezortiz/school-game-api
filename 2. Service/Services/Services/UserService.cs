﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Helpers;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class UserService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly StudentGameConfigurationService studentGameConfigurationService;
        private readonly TeacherService teacherService;
        private readonly StudentService studentService;

        public UserService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            StudentGameConfigurationService studentGameConfigurationService,
            TeacherService teacherService,
            StudentService studentService
        )
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.studentGameConfigurationService = studentGameConfigurationService;
            this.teacherService = teacherService;
            this.studentService = studentService;
        }

        public async Task<UserSvcModel> GetUserByUserId(Guid userId)
        {
            User user = await unitOfWork.UserRepository.GetAsync(userId);

            UserSvcModel userSvcModel = mapper.Map<UserSvcModel>(user);

            return userSvcModel;
        }

        public async Task<UserSvcModel> CreateNewUser(UserSvcModel userSvcModel, string name)
        {
            userSvcModel.Salt = EncryptPasswords.GenerateSalt();
            userSvcModel.Password = EncryptPasswords.GetEncryptedPassword(userSvcModel.Password, userSvcModel.Salt);

            User user = mapper.Map<User>(userSvcModel);

            unitOfWork.BeginTransaction();

            user = await unitOfWork.UserRepository.Insert(user);

            unitOfWork.SaveChanges();

            userSvcModel = mapper.Map<UserSvcModel>(user);

            if (userSvcModel.Role == "TEACHER")
            {
                await teacherService.CreateNewTeacher(
                    new TeacherSvcModel
                    {
                        UserId = userSvcModel.Id,
                        Name = name
                    });
            }
            else
            {
                var student = await studentService.CreateNewStudent(
                    new StudentSvcModel
                    {
                        UserId = userSvcModel.Id,
                        Name = name
                    });
                await studentGameConfigurationService.CreateStudentGameConfigurationWithPublicDatasets(student.Id);
                var achievements = (await unitOfWork.AchievementRepository.AllAsync()).ToList();
                foreach (Achievement achievement in achievements)
                {
                    await unitOfWork.StudentAchievementRepository
                        .Insert(new StudentAchievement()
                        {
                            AchievementId = achievement.Id,
                            CurrentValue = 0,
                            StudentId = student.Id
                        }
                    );
                }
            }
            unitOfWork.SaveChanges();

            unitOfWork.CommitTransaction();

            return userSvcModel;
        }
    }
}
