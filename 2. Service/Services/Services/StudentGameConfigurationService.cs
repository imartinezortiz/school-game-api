﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class StudentGameConfigurationService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public StudentGameConfigurationService(
            IUnitOfWork unitOfWork,
            IMapper mapper
        )
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<StudentGameConfigurationSvcModel> GetStudentGameConfigurationByStudentId(Guid studentId)
        {
            StudentGameConfiguration studentGameConfiguration = (await unitOfWork.StudentGameConfigurationRepository.FindAsync(sgc => sgc.StudentId.Equals(studentId))).FirstOrDefault();

            if (studentGameConfiguration == default) return null;

            var datasetsToRemove = new List<StudentGameConfigurationWordDataset>();
            foreach (StudentGameConfigurationWordDataset studentGameConfigurationWordDataset in studentGameConfiguration.StudentGameConfigurationWordDatasets)
            {
                List<Word> wordsToLearn = studentGameConfigurationWordDataset.WordDataset.Words.Except(studentGameConfiguration.Student.StudentLearnedWords.Select(slw => slw.Word)).ToList();
                studentGameConfigurationWordDataset.WordDataset.Words = wordsToLearn;
                if (wordsToLearn.Count == 0)
                {
                    datasetsToRemove.Add(studentGameConfigurationWordDataset);
                }
            }
            studentGameConfiguration.StudentGameConfigurationWordDatasets = studentGameConfiguration.StudentGameConfigurationWordDatasets.Except(datasetsToRemove).ToList();
            var studentGameConfigurationSvcModel = mapper.Map<StudentGameConfigurationSvcModel>(studentGameConfiguration);
            //studentGameConfiguration.LearnedWords = studentLearnedWords.Select(slw => slw.Word).ToList();

            return studentGameConfigurationSvcModel;
        }

        public async Task<StudentGameConfigurationSvcModel> CreateStudentGameConfigurationWithPublicDatasets(Guid studentId)
        {
            List<WordDataset> wordDatasets = (await unitOfWork.WordDatasetRepository.FindAsync(wd => wd.IsPublic && !wd.TeacherId.HasValue)).ToList();
            var studentGameConfiguration = new StudentGameConfiguration()
            {
                Id = Guid.Empty,
                CreatedAt = DateTimeOffset.Now,
                StudentId = studentId,
                StudentGameConfigurationWordDatasets = wordDatasets.Select(
                    wd => new StudentGameConfigurationWordDataset
                    {
                        Id = Guid.Empty,
                        CreatedAt = DateTimeOffset.Now,
                        WordDatasetId = wd.Id
                    }).ToList()
            };

            await unitOfWork.StudentGameConfigurationRepository.Insert(studentGameConfiguration);
            unitOfWork.SaveChanges();

            var studentGameConfigurationSvcModel = mapper.Map<StudentGameConfigurationSvcModel>(studentGameConfiguration);

            return studentGameConfigurationSvcModel;
        }

        public async Task UpdateStudentGameConfiguration(StudentGameConfigurationSvcModel studentGameConfigurationSvcModel)
        {
            var studenGameConfiguration = mapper.Map<StudentGameConfiguration>(studentGameConfigurationSvcModel);

            StudentGameConfiguration entityFromDb = (await unitOfWork.StudentGameConfigurationRepository.GetAsync(studentGameConfigurationSvcModel.Id));

            List<StudentGameConfigurationWordDataset> existingStudentGameConfigurationWordDatasets = (await unitOfWork.StudentGameConfigurationWordDatasetRepository
                .FindAsync(sgcwd => sgcwd.StudentGameConfigurationId == studenGameConfiguration.Id))
                .ToList();

            var deletedWordDatasetsIds = existingStudentGameConfigurationWordDatasets.Select(x => x.WordDatasetId)
                .Except(studenGameConfiguration.StudentGameConfigurationWordDatasets.Select(x => x.WordDatasetId)).ToList();
            var addedWordDatasetsIds = studenGameConfiguration.StudentGameConfigurationWordDatasets.Select(x => x.WordDatasetId)
                .Except(existingStudentGameConfigurationWordDatasets.Select(x => x.WordDatasetId)).ToList();

            unitOfWork.StudentGameConfigurationWordDatasetRepository.DeleteAll(x => deletedWordDatasetsIds.Contains(x.WordDatasetId) && x.StudentGameConfigurationId.Equals(entityFromDb.Id));
            foreach (Guid addedWordDatasetId in addedWordDatasetsIds)
            {
                await unitOfWork.StudentGameConfigurationWordDatasetRepository
                    .Insert(new StudentGameConfigurationWordDataset()
                    {
                        WordDatasetId = addedWordDatasetId,
                        StudentGameConfigurationId = entityFromDb.Id
                    });
            }

            unitOfWork.SaveChanges();
        }
    }
}
