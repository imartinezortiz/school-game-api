﻿using AutoMapper;
using Newtonsoft.Json;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class GameplayTracesAggregatorService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public GameplayTracesAggregatorService(
            IUnitOfWork unitOfWork,
            IMapper mapper
        )
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<object> GetGameplayDuration(Guid actorId)
        {
            List<GameplayEvent> gamePlayEvents = (await this.unitOfWork.GameplayEventsRepository
                    .FindAsync(ge => SchoolGameContext.JsonValue(ge.JsonEvent, "$.actor.name").Equals(actorId.ToString())))
                .ToList();

            Dictionary<string, List<Event>> jsonEvents = gamePlayEvents.ConvertAll(x => JsonConvert.DeserializeObject<Event>(x.JsonEvent))
                .GroupBy(x => x.contextAttribute.contextActivities.parent[0].id).ToDictionary(x => x.Key, x => x.ToList());

            List<KeyValuePair<string, double>> values = jsonEvents.Select(x => MaxTimestampDiferenceBetweenEvents(x.Value)).ToList();
            return GetGamplayDurationStatitisticsValues(values);
        }

        public async Task<GameStatistics> GetGameplayStatistics(Guid actorId)
        {
            List<GameplayEvent> gamePlayEvents = (await this.unitOfWork.GameplayEventsRepository
                    .FindAsync(ge => SchoolGameContext.JsonValue(ge.JsonEvent, "$.actor.name").Equals(actorId.ToString())))
                .ToList();

            Dictionary<string, List<Event>> jsonEvents = gamePlayEvents.ConvertAll(x => JsonConvert.DeserializeObject<Event>(x.JsonEvent))
                .GroupBy(x => x.contextAttribute.contextActivities.parent[0].id).ToDictionary(x => x.Key, x => x.ToList());

            List<KeyValuePair<string, double>> durationValues = jsonEvents.Select(x => MaxTimestampDiferenceBetweenEvents(x.Value)).ToList();
            FinalGameStateStatistics winLosseStatistics = GetWinLosseStatistics(
                jsonEvents
                    .Where(x => x.Value.Any(y => IsGameFinishedEvent(y)))
                    .Select(x => x.Value.First(y => IsGameFinishedEvent(y)))
                    .ToList()
            );

            return new GameStatistics
            {
                WinLosseStatistics = winLosseStatistics,
                DurationStatistics = GetGamplayDurationStatitisticsValues(durationValues)
            };
        }

        private GameDurationStatistics GetGamplayDurationStatitisticsValues(List<KeyValuePair<string, double>> values)
        {
            return new GameDurationStatistics
            {
                Min = values.Select(x => x.Value).Any() ? values.Select(x => x.Value).Min() : 0,
                Max = values.Select(x => x.Value).Any() ? values.Select(x => x.Value).Max() : 0,
                Avg = values.Select(x => x.Value).Any() ? values.Select(x => x.Value).Average() : 0,
                Total = values.Select(x => x.Value).Sum(),
                Values = values.ToDictionary(k => k.Key, v => v.Value)
            };
        }

        private KeyValuePair<string, double> MaxTimestampDiferenceBetweenEvents(List<Event> events)
        {
            return new KeyValuePair<string, double>(
                events.First(x => DateTimeOffset.Parse(x.timestamp) == events.Max(y => DateTimeOffset.Parse(y.timestamp))).timestamp,
                (events.Max(y => DateTimeOffset.Parse(y.timestamp)) -
                    events.Min(y => DateTimeOffset.Parse(y.timestamp))).TotalSeconds
            );
        }

        private FinalGameStateStatistics GetWinLosseStatistics(List<Event> events)
        {
            events = events.OrderBy(x => x.timestamp).ToList();
            List<bool> finalStatuses = events.Select(x => CastObject<bool>(x.resultAttribute["success"])).ToList();

            int maxWins = 0;
            int maxLosses = 0;
            int currentWins = 0;
            int currentLosses = 0;
            GetConsecutiveWinLosseStatistics(finalStatuses, ref maxWins, ref maxLosses, ref currentWins, ref currentLosses);

            return new FinalGameStateStatistics
            {
                Wins = finalStatuses.Count(x => x),
                Losses = finalStatuses.Count(x => !x),
                MaxConsecutiveWins = maxWins,
                MaxConsecutiveLosses = maxLosses,
                ConsecutiveWins = currentWins,
                ConsecutiveLosses = currentLosses,
                Values = events.Select(x => new { t = x.timestamp, s = CastObject<bool>(x.resultAttribute["success"]) })
                    .ToDictionary(k => k.t, v => v.s)
            };
        }

        private static void GetConsecutiveWinLosseStatistics(List<bool> finalStatuses, ref int maxWins, ref int maxLosses, ref int currentWins, ref int currentLosses)
        {
            for (int i = 0; i < finalStatuses.Count; i++)
            {
                if (finalStatuses[i])
                {
                    if (currentLosses > 0) currentLosses = 0;
                    currentWins++;
                    maxWins = Math.Max(maxWins, currentWins);
                }
                else
                {
                    if (currentWins > 0) currentWins = 0;
                    currentLosses++;
                    maxLosses = Math.Max(maxLosses, currentLosses);
                }
            }
        }

        private bool IsGameFinishedEvent(Event value)
        {
            return
                value.resultAttribute.ContainsKey("success") &&
                value.verbAttribute.id.Contains("completed") &&
                value.resultAttribute.ContainsKey("extensions") &&
                CastObject<Dictionary<string, object>>(value.resultAttribute["extensions"]).Keys.Any(k => k.Contains("finish-reason"));

        }

        private T CastObject<T>(object obj)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(obj));
        }
    }
}
