﻿using AutoMapper;
using SchoolGame.Entities;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services.Achievements;
using SchoolGame.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class GameplayEventService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public GameplayEventService(
            IUnitOfWork unitOfWork,
            IMapper mapper
        )
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<List<GameplayEventSvcModel>> GetGameplayEventsByGameplayId(Guid gamplayId)
        {
            List<GameplayEvent> gameplayEvents = (await unitOfWork.GameplayEventsRepository
                    .FindAsync(ge => SchoolGameContext.JsonValue(ge.JsonEvent, "$.context.contextActivities.parent[0].id").EndsWith(gamplayId.ToString())))
                .ToList();

            var gameplayEventsSvcModel = mapper.Map<List<GameplayEventSvcModel>>(gameplayEvents);

            return gameplayEventsSvcModel;
        }

        public async Task<GameplayEventSvcModel> CreateNewGameplayEvent(GameplayEventSvcModel gameplayEventSvcModel)
        {
            var gameplayEvent = mapper.Map<GameplayEvent>(gameplayEventSvcModel);
            gameplayEvent = await unitOfWork.GameplayEventsRepository.Insert(gameplayEvent);

            unitOfWork.SaveChanges();

            gameplayEventSvcModel = mapper.Map<GameplayEventSvcModel>(gameplayEvent);

            return gameplayEventSvcModel;
        }
    }
}
