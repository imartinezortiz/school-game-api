﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Helpers;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class SchoolGameAuthenticationService
    {
        private readonly IMapper mapper;
        private IUnitOfWork unitOfWork;
        private readonly IConfiguration configuration;

        public SchoolGameAuthenticationService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }

        public async Task<UserSvcModel> GetUserFromAuthenticationSvcModel(AuthenticationSvcModel userAuthenticated)
        {
            var user = (await unitOfWork.UserRepository.FindAsync(u => u.Email == userAuthenticated.Login)).FirstOrDefault();
            var userSvcModel = mapper.Map<UserSvcModel>(user);
            return userSvcModel;
        }

        public async Task<AuthenticationSvcModel> AuthenticateUser(AuthenticationSvcModel authenticationSvcModel)
        {
            User user = (await this.unitOfWork.UserRepository.FindAsync(u => u.Email == authenticationSvcModel.Login)).FirstOrDefault();

            if (user == null)
            {
                return null;
            }

            var userSvcModel = mapper.Map<UserSvcModel>(user);

            string encryptedPassword = EncryptPasswords.GetEncryptedPassword(authenticationSvcModel.Password, userSvcModel.Salt);

            authenticationSvcModel.Token = null;

            if (userSvcModel.Password == encryptedPassword)
            {
                authenticationSvcModel.Token = GenerateJwtToken(user);
            }

            return authenticationSvcModel;
        }

        public AuthenticationSvcModel GetAuthenticantionUserByUserId(Guid userId)
        {
            User user = (unitOfWork.UserRepository.Find(u => u.Id == userId)).FirstOrDefault();
            var userSvcModel = mapper.Map<AuthenticationSvcModel>(user);

            return userSvcModel;
        }

        private string GenerateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.ASCII.GetBytes(configuration.GetValue<string>("Jwt:Secret"));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim("id", user.Id.ToString()),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(100),
                Issuer = configuration.GetValue<string>("Jwt:Issuer"),
                Audience = configuration.GetValue<string>("Jwt:Issuer"),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
