﻿using SchoolGame.Services.Models;

namespace SchoolGame.Services.Services.Achievements
{
    public class NullAchievement : IAchievementResolver
    {
        public bool IsAchieved(StudentAchievementSvcModel studentAchievementSvcModel)
        {
            return true;
        }

        public void UpdateCurrentValue(StudentAchievementSvcModel studentAchievementSvcModel)
        {
            return;
        }
    }
}
