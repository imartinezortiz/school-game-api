﻿using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;

namespace SchoolGame.Services.Services.Achievements
{
    public class AchieveNGameplays : IAchievementResolver
    {
        private readonly IUnitOfWork unitOfWork;

        public AchieveNGameplays(
            IUnitOfWork unitOfWork
        )
        {
            this.unitOfWork = unitOfWork;
        }

        public bool IsAchieved(StudentAchievementSvcModel studentAchievementSvcModel)
        {
            return studentAchievementSvcModel.CurrentValue == studentAchievementSvcModel.Achievement.MaxValue;
        }

        public void UpdateCurrentValue(StudentAchievementSvcModel studentAchievementSvcModel)
        {
            if (!IsAchieved(studentAchievementSvcModel))
            {
                StudentAchievement studentAchievementFromDB = this.unitOfWork.StudentAchievementRepository.Get(studentAchievementSvcModel.Id);
                studentAchievementFromDB.CurrentValue++;
                this.unitOfWork.StudentAchievementRepository.Update(studentAchievementFromDB);
                this.unitOfWork.StudentAchievementLogRepository.Insert(
                    new StudentAchievementLog()
                    {
                        StudentAchievementId = studentAchievementFromDB.Id,
                        CurrentValue = studentAchievementFromDB.CurrentValue
                    });
            }
        }
    }
}
