﻿using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System.Linq;

namespace SchoolGame.Services.Services.Achievements
{
    public class AchieveNConsecutiveWins : IAchievementResolver
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly GameplaySvcModel gameplay;

        public AchieveNConsecutiveWins(
            IUnitOfWork unitOfWork,
            GameplaySvcModel gameplay
        )
        {
            this.unitOfWork = unitOfWork;
            this.gameplay = gameplay;
        }

        public bool IsAchieved(StudentAchievementSvcModel studentAchievementSvcModel)
        {
            return studentAchievementSvcModel.CurrentValue == studentAchievementSvcModel.Achievement.MaxValue;
        }

        public void UpdateCurrentValue(StudentAchievementSvcModel studentAchievementSvcModel)
        {
            if (!IsAchieved(studentAchievementSvcModel) && this.gameplay.IsObjectiveAchieved)
            {
                StudentAchievement studentAchievementFromDB = this.unitOfWork.StudentAchievementRepository.Get(studentAchievementSvcModel.Id);
                studentAchievementFromDB.CurrentValue++;
                this.unitOfWork.StudentAchievementRepository.Update(studentAchievementFromDB);
                this.unitOfWork.StudentAchievementLogRepository.Insert(
                    new StudentAchievementLog()
                    {
                        StudentAchievementId = studentAchievementFromDB.Id,
                        CurrentValue = studentAchievementFromDB.CurrentValue
                    });
            }
            else
            {
                StudentAchievement studentAchievementFromDB = this.unitOfWork.StudentAchievementRepository.Get(studentAchievementSvcModel.Id);
                int lastCurrentValue = studentAchievementFromDB.CurrentValue;
                studentAchievementFromDB.CurrentValue = 0;
                this.unitOfWork.StudentAchievementRepository.Update(studentAchievementFromDB);
                if (lastCurrentValue != 0)
                {
                    this.unitOfWork.StudentAchievementLogRepository.Insert(
                        new StudentAchievementLog()
                        {
                            StudentAchievementId = studentAchievementFromDB.Id,
                            CurrentValue = studentAchievementFromDB.CurrentValue
                        });
                }
                
            }
        }
    }
}
