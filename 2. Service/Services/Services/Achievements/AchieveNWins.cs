﻿using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;

namespace SchoolGame.Services.Services.Achievements
{
    public class AchieveNWins : IAchievementResolver
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly GameplaySvcModel gameplay;

        public AchieveNWins(
            IUnitOfWork unitOfWork,
            GameplaySvcModel gameplay
        )
        {
            this.unitOfWork = unitOfWork;
            this.gameplay = gameplay;
        }

        public bool IsAchieved(StudentAchievementSvcModel studentAchievementSvcModel)
        {
            return studentAchievementSvcModel.CurrentValue == studentAchievementSvcModel.Achievement.MaxValue;
        }

        public void UpdateCurrentValue(StudentAchievementSvcModel studentAchievementSvcModel)
        {
            if (!IsAchieved(studentAchievementSvcModel) && this.gameplay.IsObjectiveAchieved)
            {
                StudentAchievement studentAchievementFromDB = this.unitOfWork.StudentAchievementRepository.Get(studentAchievementSvcModel.Id);
                studentAchievementFromDB.CurrentValue++;
                this.unitOfWork.StudentAchievementRepository.Update(studentAchievementFromDB);
                this.unitOfWork.StudentAchievementLogRepository.Insert(
                    new StudentAchievementLog()
                    {
                        StudentAchievementId = studentAchievementFromDB.Id,
                        CurrentValue = studentAchievementFromDB.CurrentValue
                    });
            }
        }
    }
}
