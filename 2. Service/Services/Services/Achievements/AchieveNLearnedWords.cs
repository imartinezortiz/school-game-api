﻿using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System.Linq;

namespace SchoolGame.Services.Services.Achievements
{
    public class AchieveNLearnedWords : IAchievementResolver
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly GameplaySvcModel gameplay;

        public AchieveNLearnedWords(
            IUnitOfWork unitOfWork,
            GameplaySvcModel gameplay
        )
        {
            this.unitOfWork = unitOfWork;
            this.gameplay = gameplay;
        }

        public bool IsAchieved(StudentAchievementSvcModel studentAchievementSvcModel)
        {
            return studentAchievementSvcModel.CurrentValue == studentAchievementSvcModel.Achievement.MaxValue;
        }

        public void UpdateCurrentValue(StudentAchievementSvcModel studentAchievementSvcModel)
        {
            if (!IsAchieved(studentAchievementSvcModel) && this.gameplay.IsObjectiveAchieved &&
                studentAchievementSvcModel.CurrentValue < studentAchievementSvcModel.Student.StudentLearnedWords.Count
            )
            {
                StudentAchievement studentAchievementFromDB = this.unitOfWork.StudentAchievementRepository.Get(studentAchievementSvcModel.Id);
                studentAchievementFromDB.CurrentValue = studentAchievementSvcModel.Student.StudentLearnedWords.Count;
                this.unitOfWork.StudentAchievementRepository.Update(studentAchievementFromDB);
                this.unitOfWork.StudentAchievementLogRepository.Insert(
                    new StudentAchievementLog()
                    {
                        StudentAchievementId = studentAchievementFromDB.Id,
                        CurrentValue = studentAchievementFromDB.CurrentValue
                    });
            }
        }
    }
}
