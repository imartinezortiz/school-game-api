﻿using SchoolGame.Services.Models;

namespace SchoolGame.Services.Services.Achievements
{
    public interface IAchievementResolver
    {
        bool IsAchieved(StudentAchievementSvcModel studentAchievementSvcModel);
        void UpdateCurrentValue(StudentAchievementSvcModel studentAchievementSvcModel);
    }
}
