﻿using AutoMapper;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;

namespace SchoolGame.Services.Services.Achievements
{
    public class AchievementFactory
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AchievementFactory(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public IAchievementResolver GetAchievementResolver(string resolverName, GameplaySvcModel currentGameplay)
        {
            return resolverName switch
            {
                "AchieveNGameplays" => new AchieveNGameplays(this.unitOfWork),
                "AchieveNWins" => new AchieveNWins(this.unitOfWork, currentGameplay),
                "AchieveNLearnedWords" => new AchieveNLearnedWords(this.unitOfWork, currentGameplay),
                "AchieveNConsecutiveWins" => new AchieveNConsecutiveWins(this.unitOfWork, currentGameplay),
                _ => new NullAchievement(),
            };
        }
    }
}
