﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Helpers;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class TeacherService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TeacherService(
            IUnitOfWork unitOfWork,
            IMapper mapper
        )
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<TeacherSvcModel> GetTeacherByTeacherId(Guid teacherId)
        {
            Teacher teacher = await unitOfWork.TeacherRepository.GetAsync(teacherId);

            TeacherSvcModel teacherSvcModel = mapper.Map<TeacherSvcModel>(teacher);

            return teacherSvcModel;
        }

        public async Task<TeacherSvcModel> GetTeacherByUserId(Guid userId)
        {
            Teacher teacher = (await unitOfWork.TeacherRepository.FindAsync(t => t.UserId.Equals(userId))).FirstOrDefault();

            TeacherSvcModel teacherSvcModel = mapper.Map<TeacherSvcModel>(teacher);

            return teacherSvcModel;
        }

        public async Task<TeacherSvcModel> CreateNewTeacher(TeacherSvcModel teacherSvcModel)
        {
            Student student = (await unitOfWork.StudentRepository.FindAsync(s => s.UserId.Equals(teacherSvcModel.UserId))).FirstOrDefault();

            if (student != default) return null;

            Teacher teacher = mapper.Map<Teacher>(teacherSvcModel);
            teacher = await unitOfWork.TeacherRepository.Insert(teacher);

            unitOfWork.SaveChanges();

            teacherSvcModel = mapper.Map<TeacherSvcModel>(teacher);

            return teacherSvcModel;
        }
    }
}
