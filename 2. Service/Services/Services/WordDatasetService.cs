﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Helpers;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class WordDatasetService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public WordDatasetService(
            IUnitOfWork unitOfWork,
            IMapper mapper
        )
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<WordDatasetSvcModel> GetWordDatasetById(Guid id)
        {
            WordDataset wordDataset = (await unitOfWork.WordDatasetExtendedRepository.FindAsync(wd => wd.Id.Equals(id) && !wd.DeletedAt.HasValue)).FirstOrDefault();

            var wordDatasetSvcModel = mapper.Map<WordDatasetSvcModel>(wordDataset);

            wordDatasetSvcModel.StudentIds = (await unitOfWork.StudentGameConfigurationWordDatasetExtendedRepository
                .FindAsync(sgcwd => sgcwd.WordDatasetId.Equals(wordDatasetSvcModel.Id) && !sgcwd.DeletedAt.HasValue))
                .Select(sgcwd => sgcwd.StudentGameConfiguration.StudentId)
                .ToList();

            return wordDatasetSvcModel;
        }

        public async Task<List<WordDatasetSvcModel>> GetWordDatasetsByTeacherId(Guid teacherId)
        {
            List<WordDataset> wordDatasets = (await unitOfWork.WordDatasetExtendedRepository.FindAsync(wd => (wd.TeacherId.Equals(teacherId) || wd.IsPublic) && !wd.DeletedAt.HasValue))
                .AsQueryable().ToList();

            var wordDatasetsSvcModel = mapper.Map<List<WordDatasetSvcModel>>(wordDatasets);

            return wordDatasetsSvcModel;
        }

        public async Task<WordDatasetSvcModel> CreateNewWordDataset(WordDatasetSvcModel wordDatasetSvcModel)
        {
            var wordDataset = mapper.Map<WordDataset>(wordDatasetSvcModel);

            wordDataset = await unitOfWork.WordDatasetRepository.Insert(wordDataset);

            List<StudentGameConfiguration> studentGameConfigurations = unitOfWork.StudentGameConfigurationRepository.Find(sgc => wordDatasetSvcModel.StudentIds.Any(id => sgc.StudentId.Equals(id))).ToList();

            foreach (Guid studentId in wordDatasetSvcModel.StudentIds)
            {
                await unitOfWork.StudentGameConfigurationWordDatasetRepository.Insert(new StudentGameConfigurationWordDataset()
                {
                    WordDataset = wordDataset,
                    StudentGameConfigurationId = studentGameConfigurations.First(sgc => sgc.StudentId.Equals(studentId)).Id
                });
            }

            unitOfWork.SaveChanges();

            wordDatasetSvcModel = mapper.Map<WordDatasetSvcModel>(wordDataset);

            return wordDatasetSvcModel;
        }

        public async Task<WordDatasetSvcModel> UpdateWordDataset(WordDatasetSvcModel wordDatasetSvcModel)
        {
            var wordDataset = mapper.Map<WordDataset>(wordDatasetSvcModel);
            wordDataset.StudentGameConfigurationWordDatasets = null;
            wordDataset = unitOfWork.WordDatasetRepository.Update(wordDataset);

            await ManageStudentGameConfiguraionWordDatasetUpdates(wordDatasetSvcModel);

            unitOfWork.SaveChanges();

            wordDatasetSvcModel = mapper.Map<WordDatasetSvcModel>(wordDataset);

            return wordDatasetSvcModel;
        }

        public async Task SoftDeleteWordDataset(Guid wordDatasetId)
        {
            WordDataset wordDataset = await unitOfWork.WordDatasetRepository.GetAsync(wordDatasetId);

            if (!wordDataset.DeletedAt.HasValue)
            {
                wordDataset.DeletedAt = DateTimeOffset.Now;
                unitOfWork.WordDatasetRepository.Update(wordDataset);

                unitOfWork.SaveChanges();
            }
        }

        private async Task ManageStudentGameConfiguraionWordDatasetUpdates(WordDatasetSvcModel wordDatasetSvcModel)
        {
            await InsertStudentGameConfiguraionWordDataset(wordDatasetSvcModel);

            await SoftDeleteStudentGameConfigurationWordDatasets(wordDatasetSvcModel);

            UnsoftDeleteStudentGameConfigurationWordDatasets(wordDatasetSvcModel);
        }

        private async Task InsertStudentGameConfiguraionWordDataset(WordDatasetSvcModel wordDatasetSvcModel)
        {
            List<StudentGameConfiguration> studentGameConfigurationToCreate = unitOfWork.StudentGameConfigurationRepository
                            .Find(sgc => wordDatasetSvcModel.StudentIds.Any(id => sgc.StudentId.Equals(id)) && !sgc.StudentGameConfigurationWordDatasets.Any(x => x.WordDatasetId.Equals(wordDatasetSvcModel.Id)))
                            .ToList();

            foreach (StudentGameConfiguration studentGameConfiguration in studentGameConfigurationToCreate)
            {
                await unitOfWork.StudentGameConfigurationWordDatasetRepository.Insert(new StudentGameConfigurationWordDataset()
                {

                    WordDatasetId = wordDatasetSvcModel.Id,
                    StudentGameConfigurationId = studentGameConfiguration.Id
                });
            }
        }

        private async Task SoftDeleteStudentGameConfigurationWordDatasets(WordDatasetSvcModel wordDatasetSvcModel)
        {
            List<StudentGameConfigurationWordDataset> studentGameConfigurationWordDatasetsToSoftDelete = (await unitOfWork.StudentGameConfigurationWordDatasetExtendedRepository
                            .FindAsync(sgcwd => wordDatasetSvcModel.Id.Equals(sgcwd.WordDatasetId)))
                            .Where(sgcwd => !wordDatasetSvcModel.StudentIds.Any(studentId => sgcwd.StudentGameConfiguration.StudentId.Equals(studentId)))
                            .ToList();

            foreach (StudentGameConfigurationWordDataset studentGameConfigurationWordDataset in studentGameConfigurationWordDatasetsToSoftDelete)
            {
                studentGameConfigurationWordDataset.DeletedAt = DateTimeOffset.Now;
                unitOfWork.StudentGameConfigurationWordDatasetRepository.Update(studentGameConfigurationWordDataset);
            }
        }

        private void UnsoftDeleteStudentGameConfigurationWordDatasets(WordDatasetSvcModel wordDatasetSvcModel)
        {
            List<StudentGameConfigurationWordDataset> studentGameConfigurationToUndelete = unitOfWork.StudentGameConfigurationRepository
                            .Find(sgc =>
                                wordDatasetSvcModel.StudentIds.Any(id => sgc.StudentId.Equals(id)) &&
                                sgc.StudentGameConfigurationWordDatasets.Any(sgcwd => sgcwd.DeletedAt != null && sgcwd.WordDatasetId.Equals(wordDatasetSvcModel.Id)))
                            .Select(sgc => sgc.StudentGameConfigurationWordDatasets.First(sgcwd => sgcwd.DeletedAt != null && sgcwd.WordDatasetId.Equals(wordDatasetSvcModel.Id)))
                            .ToList();

            foreach (StudentGameConfigurationWordDataset studentGameConfigurationWordDataset in studentGameConfigurationToUndelete)
            {
                studentGameConfigurationWordDataset.DeletedAt = null;
                unitOfWork.StudentGameConfigurationWordDatasetRepository.Update(studentGameConfigurationWordDataset);
            }
        }
    }
}
