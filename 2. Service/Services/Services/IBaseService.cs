﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public interface IBaseService<T>
        where T : BaseSvcModel
    {
        Task<T> GetById(Guid entityId);

        Task<T> Create(T entitySvcModel);

        T UpdateClassroom(T entitySvcModel);

        Task DeleteClassroom(Guid entityId);
    }
}
