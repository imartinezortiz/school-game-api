﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class StudentClassroomService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public StudentClassroomService(
            IUnitOfWork unitOfWork,
            IMapper mapper
        )
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<StudentClassroomSvcModel> GetStudentClassroomByStudentId(Guid studentId)
        {
            StudentClassroom studentClassroom = (await unitOfWork.StudentClassroomRepository.FindAsync(sc => sc.StudentId.Equals(studentId))).FirstOrDefault();

            if (studentClassroom == default) return null;

            StudentClassroomSvcModel studentClassroomSvcModel = mapper.Map<StudentClassroomSvcModel>(studentClassroom);

            return studentClassroomSvcModel;
        }

        public async Task<List<StudentClassroomSvcModel>> GetStudentClassroomsByTeacherId(Guid teacherId)
        {
            List<StudentClassroom> studentClassrooms = (await unitOfWork.StudentClassroomRepository
                .FindAsync(sc => sc.Classroom.ClassroomsTeachers.Any(ct => ct.TeacherId.Equals(teacherId)))).ToList();

            if (studentClassrooms == default) return null;

            var studentClassroomsSvcModel = mapper.Map<List<StudentClassroomSvcModel>>(studentClassrooms);

            return studentClassroomsSvcModel;
        }

        public async Task<StudentClassroomSvcModel> CreateNewStudentClassroom(StudentClassroomSvcModel studentClassroomSvcModel, string code)
        {
            Guid classroomId = (await unitOfWork.ClassroomRepository.FindAsync(c => c.Code.Equals(code)))
                .Select(c => c.Id).FirstOrDefault();

            if (classroomId == default) return null;

            Guid studentClassroomStatusId = (await unitOfWork.StudentClassroomStatusRepository.FindAsync(c => c.Code == (int)StudentClassroomStatusEnum.PENDING))
                .Select(c => c.Id).FirstOrDefault();

            StudentClassroom studentClassroom = mapper.Map<StudentClassroom>(studentClassroomSvcModel);
            studentClassroom.ClassroomId = classroomId;
            studentClassroom.StudentClassroomStatusId = studentClassroomStatusId;
            studentClassroom = await unitOfWork.StudentClassroomRepository.Insert(studentClassroom);

            unitOfWork.SaveChanges();

            studentClassroomSvcModel = mapper.Map<StudentClassroomSvcModel>(studentClassroom);

            return studentClassroomSvcModel;
        }

        public async Task<StudentClassroomSvcModel> UpdateStudentClassroom(StudentClassroomSvcModel studentClassroomSvcModel)
        {
            StudentClassroom studentClassroomFromDb = (await this.unitOfWork.StudentClassroomRepository.FindAsync(sc => sc.Id.Equals(studentClassroomSvcModel.Id)))
                .FirstOrDefault();

            if (studentClassroomFromDb == default) return null;

            if (studentClassroomFromDb.StudentClassroomStatus.Code != studentClassroomSvcModel.StudentClassroomStatusCode)
            {
                StudentClassroomStatus studentClassroomStatus = (await this.unitOfWork.StudentClassroomStatusRepository.FindAsync(c => c.Code == studentClassroomSvcModel.StudentClassroomStatusCode))
                    .FirstOrDefault();

                if (studentClassroomStatus == default) return null;

                studentClassroomFromDb.StudentClassroomStatusId = studentClassroomStatus.Id;
            }

            studentClassroomFromDb = this.unitOfWork.StudentClassroomRepository.Update(studentClassroomFromDb);
            unitOfWork.SaveChanges();

            return mapper.Map<StudentClassroomSvcModel>(studentClassroomFromDb);
        }

        public async Task<bool> DeleteStudentClassroom(Guid studentClassroomId)
        {
            await unitOfWork.StudentClassroomRepository.DeleteAsync(studentClassroomId);
            unitOfWork.SaveChanges();
            return true;
        }
    }
}
