﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services.Achievements;
using SchoolGame.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class GameplayService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly AchievementFactory achievementFactory;

        public GameplayService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            AchievementFactory achievementFactory
        )
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.achievementFactory = achievementFactory;
        }

        public async Task<GameplaySvcModel> GetGameplayByGameplayId(Guid gamplayId)
        {
            Gameplay gamplay = await unitOfWork.GameplaysRepository.GetAsync(gamplayId);

            var gameplaySvcModel = mapper.Map<GameplaySvcModel>(gamplay);

            return gameplaySvcModel;
        }

        public async Task<GameplaySvcModel> CreateNewGameplay(GameplaySvcModel gameplaySvcModel)
        {
            var gameplay = mapper.Map<Gameplay>(gameplaySvcModel);
            gameplay = await unitOfWork.GameplaysRepository.Insert(gameplay);

            unitOfWork.SaveChanges();

            gameplaySvcModel = mapper.Map<GameplaySvcModel>(gameplay);

            return gameplaySvcModel;
        }

        public GameplaySvcModel UpdateGameplay(GameplaySvcModel gameplaySvcModel)
        {
            Gameplay gameplay = unitOfWork.GameplaysRepository.GetQueryable().Where(g => g.Id.Equals(gameplaySvcModel.Id) && g.Student.UserId.Equals(gameplaySvcModel.UserId)).FirstOrDefault();
            gameplay.EndDate = gameplaySvcModel.EndDate;
            gameplay.IsObjectiveAchieved = gameplaySvcModel.IsObjectiveAchieved;
            gameplay.Score = gameplaySvcModel.Score;
            gameplay.Level = gameplaySvcModel.Level;
            gameplay = unitOfWork.GameplaysRepository.Update(gameplay);

            StudentGameConfiguration studentGameConfiguration = this.unitOfWork.StudentGameConfigurationRepository.Find(sgc => sgc.StudentId.Equals(gameplay.StudentId)).FirstOrDefault();
            ManageLevelChanges(gameplay, studentGameConfiguration);
            ManageHighScore(gameplay.Score, studentGameConfiguration);
            ManageLearnedWordsChanges(gameplay);
            MagageAchievementsChanges(gameplaySvcModel);

            unitOfWork.SaveChanges();

            gameplaySvcModel = mapper.Map<GameplaySvcModel>(gameplay);

            return gameplaySvcModel;
        }

        private void ManageLearnedWordsChanges(Gameplay gameplay)
        {
            if (gameplay.IsObjectiveAchieved && !IsWordLearnedPreviously(gameplay))
            {
                unitOfWork.StudentLearnedWordRepository.Insert(new StudentLearnedWord()
                {
                    StudentId = gameplay.StudentId,
                    WordId = gameplay.ObjectiveId
                });
            }
        }

        private void MagageAchievementsChanges(GameplaySvcModel gameplaySvcModel)
        {
            List<StudentAchievement> studentAchievements = unitOfWork.StudentAchievementRepository.GetQueryable()
                            .Where(sa => sa.Student.UserId.Equals(gameplaySvcModel.UserId) && sa.CurrentValue != sa.Achievement.MaxValue).ToList();

            foreach (StudentAchievement studentAchievement in studentAchievements)
            {
                var studentAchievementSvcModel = mapper.Map<StudentAchievementSvcModel>(studentAchievement);
                achievementFactory.GetAchievementResolver(studentAchievement.Achievement.AchievementCheckClass, gameplaySvcModel).UpdateCurrentValue(studentAchievementSvcModel);
            }
        }

        private void ManageLevelChanges(Gameplay gameplay, StudentGameConfiguration studentGameConfiguration)
        {
            if (studentGameConfiguration.Level != gameplay.Level)
            {
                studentGameConfiguration.Level = gameplay.Level;
                studentGameConfiguration.SuggestChangeLevelValue = 0;
            }
            else
            {
                studentGameConfiguration.SuggestChangeLevelValue += gameplay.IsObjectiveAchieved ? 1 : -1;
            }

            this.unitOfWork.StudentGameConfigurationRepository.Update(studentGameConfiguration);
        }

        private void ManageHighScore(int currentScore, StudentGameConfiguration studentGameConfiguration)
        {
            if(currentScore > studentGameConfiguration.HighScore)
            {
                studentGameConfiguration.HighScore = currentScore;
            }
            this.unitOfWork.StudentGameConfigurationRepository.Update(studentGameConfiguration);
        }

        private bool IsWordLearnedPreviously(Gameplay gameplay) =>
            unitOfWork.StudentLearnedWordRepository
                .Find(slw => slw.WordId.Equals(gameplay.ObjectiveId) && slw.StudentId.Equals(gameplay.StudentId))
                .Any();
    }
}
