﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using SchoolGame.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolGame.Services.Services
{
    public class StudentService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        private readonly int STUDENT_CLASSROOM_ACCEPTED_REQUESTE_CODE = 1;

        public StudentService(
            IUnitOfWork unitOfWork,
            IMapper mapper
        )
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<StudentSvcModel> GetStudentById(Guid studentId)
        {
            Student student = (await unitOfWork.StudentRepository.FindAsync(s => s.Id.Equals(studentId))).FirstOrDefault();

            StudentSvcModel studentSvcModel = mapper.Map<StudentSvcModel>(student);

            return studentSvcModel;
        }

        public async Task<StudentSvcModel> GetStudentByUserId(Guid userId)
        {
            Student student = (await unitOfWork.StudentRepository.FindAsync(t => t.UserId.Equals(userId))).FirstOrDefault();

            StudentSvcModel studentSvcModel = mapper.Map<StudentSvcModel>(student);

            return studentSvcModel;
        }

        public async Task<StudentSvcModel> CreateNewStudent(StudentSvcModel studentSvcModel)
        {
            Teacher teacher = (await unitOfWork.TeacherRepository.FindAsync(t => t.UserId.Equals(studentSvcModel.UserId))).FirstOrDefault();

            if (teacher != default) return null;

            Student student = mapper.Map<Student>(studentSvcModel);
            student = await unitOfWork.StudentRepository.Insert(student);

            unitOfWork.SaveChanges();

            studentSvcModel = mapper.Map<StudentSvcModel>(student);

            return studentSvcModel;
        }

        public async Task<List<StudentSvcModel>> GetStudentsByTeacherId(Guid teacherId)
        {
            List<Student> students = (await unitOfWork.StudentRepository
                .FindAsync(
                    s =>
                        s.StudentClassroom.Classroom.ClassroomsTeachers.Any(ct => ct.TeacherId.Equals(teacherId))
                        && s.StudentClassroom.StudentClassroomStatus.Code.Equals(STUDENT_CLASSROOM_ACCEPTED_REQUESTE_CODE))
                ).ToList();

            return mapper.Map<List<StudentSvcModel>>(students);
        }

        public async Task<bool> DeleteStudentById(Guid studentId)
        {
            try
            {
                await unitOfWork.StudentRepository.DeleteAsync(studentId);
                unitOfWork.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                var algo = ex;
                return false;
            }
        }
    }
}
