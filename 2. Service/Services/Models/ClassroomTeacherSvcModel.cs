﻿
using System;

namespace SchoolGame.Services.Models
{
    public class ClassroomTeacherSvcModel
    {
        public Guid ClassroomId { get; set; }
        public Guid TeacherId { get; set; }
    }
}
