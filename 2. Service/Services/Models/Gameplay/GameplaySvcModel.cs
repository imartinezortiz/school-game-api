﻿using System;
using System.Collections.Generic;

namespace SchoolGame.Services.Models
{
    public class GameplaySvcModel : BaseSvcModel
    {
        public StudentSvcModel Student { get; set; }

        public WordSvcModel Objective { get; set; }

        public DateTimeOffset StartDate { get; set; } = DateTimeOffset.Now;

        public DateTimeOffset? EndDate { get; set; }

        public int Level { get; set; } = 0;

        public int Score { get; set; } = 0;

        public bool IsObjectiveAchieved { get; set; } = false;
        public Guid UserId { get; set; }

        public List<GameplayEventSvcModel> GameplayEvents { get; set; }

    }
}
