﻿
using System;

namespace SchoolGame.Services.Models
{
    public class GameplayEventSvcModel : BaseSvcModel
    {
        public Guid GameplayId { get; set; }
        public GameplaySvcModel Gameplay { get; set; }

        public string JsonEvent { get; set; } 
    }
}
