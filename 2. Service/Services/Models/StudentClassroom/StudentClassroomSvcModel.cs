﻿using System;

namespace SchoolGame.Services.Models
{
    public class StudentClassroomSvcModel
    {
        public Guid Id { get; set; }
        public int StudentClassroomStatusCode { get; set; }
        public string Message { get; set; }
        public Guid ClassroomId { get; set; }
        public ClassroomSvcModel Classroom { get; set; }
        public Guid StudentId { get; set; }
        public StudentSvcModel Student { get; set; }
    }
}
