﻿
using System;

namespace SchoolGame.Services.Models
{
    public class UserSvcModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Role { get; set; }
    }
}
