﻿using System;
using System.Collections.Generic;

namespace SchoolGame.Services.Models
{
    public class GameDurationStatistics
    {
        public double Min { get; set; }
        public double Max { get; set; }
        public double Avg { get; set; }
        public double Total { get; set; }
        public Dictionary<string, double> Values { get; set; }
    }
}
