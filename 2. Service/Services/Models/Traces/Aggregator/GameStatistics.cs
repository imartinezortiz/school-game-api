﻿namespace SchoolGame.Services.Models
{
    public class GameStatistics
    {
        public FinalGameStateStatistics WinLosseStatistics { get; set; }
        public GameDurationStatistics DurationStatistics { get; set; }
    }
}
