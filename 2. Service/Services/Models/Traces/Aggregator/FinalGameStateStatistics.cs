﻿using System;
using System.Collections.Generic;

namespace SchoolGame.Services.Models
{
    public class FinalGameStateStatistics
    {
        public int Wins { get; set; }
        public int Losses { get; set; }
        public int ConsecutiveWins { get; set; }
        public int ConsecutiveLosses { get; set; }
        public int MaxConsecutiveWins { get; set; }
        public int MaxConsecutiveLosses { get; set; }
        public Dictionary<string, bool> Values { get; set; }
    }
}
