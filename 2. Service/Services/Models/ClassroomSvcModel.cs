﻿
using System;
using System.Collections.Generic;

namespace SchoolGame.Services.Models
{
    public class ClassroomSvcModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SchoolName { get; set; }
        public string SchoolGrade { get; set; }
        public string Code { get; set; }
        public List<Guid> TeachersIds { get; set; }
    }
}
