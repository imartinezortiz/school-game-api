﻿
using System;

namespace SchoolGame.Services.Models
{
    public class TeacherSvcModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid UserId { get; set; }
        public UserSvcModel User { get; set; }
    }
}
