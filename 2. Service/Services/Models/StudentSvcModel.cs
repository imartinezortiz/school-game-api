﻿using System;
using System.Collections.Generic;

namespace SchoolGame.Services.Models
{
    public class StudentSvcModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid? ClassroomId { get; set; }
        public ClassroomSvcModel Classroom { get; set; }

        public Guid UserId { get; set; }
        public UserSvcModel User { get; set; }
        public List<StudentAchievementSvcModel> StudentAchievements { get; set; }
        public List<WordSvcModel> StudentLearnedWords { get; set; }
    }
}
