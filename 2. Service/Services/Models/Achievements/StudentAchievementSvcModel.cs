﻿using System;

namespace SchoolGame.Services.Models
{
    public class StudentAchievementSvcModel
    {
        public Guid Id { get; set; }

        public Guid StudentId { get; set; }
        public virtual StudentSvcModel Student { get; set; }

        public Guid AchievementId { get; set; }
        public virtual AchievementSvcModel Achievement { get; set; }

        public int CurrentValue { get; set; }
    }
}
