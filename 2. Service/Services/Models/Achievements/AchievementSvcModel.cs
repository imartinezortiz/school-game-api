﻿using System;

namespace SchoolGame.Services.Models
{
    public class AchievementSvcModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string AchievementCheckClass { get; set; }

        public int Order { get; set; }

        public int MaxValue { get; set; }
    }
}
