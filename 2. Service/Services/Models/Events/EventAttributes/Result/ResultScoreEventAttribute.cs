﻿using Newtonsoft.Json;

namespace SchoolGame.Services.Models
{
    public class ResultScoreEventAttribute
    {
        [JsonProperty(PropertyName = "scaled")]
        public float scaledAttribute;

        [JsonProperty(PropertyName = "raw")]
        public float rawAttribute;

        [JsonProperty(PropertyName = "min")]
        public float minAttribute;

        [JsonProperty(PropertyName = "max")]
        public float maxAttribute;
    }
}