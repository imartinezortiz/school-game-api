﻿namespace SchoolGame.Services.Models
{
    public class ObjectEventAttribute
    {
        public string id;
        public ObjectDefinitionEventAttribute definition;
    }
}