﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SchoolGame.Services.Models
{
    [Serializable]
    public class Event
    {
        [JsonProperty(PropertyName = "actor")]
        public ActorEventAttribute actorAttribute;

        [JsonProperty(PropertyName = "verb")]
        public VerbEventAttribute verbAttribute;

        [JsonProperty(PropertyName = "object")]
        public ObjectEventAttribute objectAttribute;

        [JsonProperty(PropertyName = "result")]
        public Dictionary<string, object> resultAttribute = new Dictionary<string, object>();

        [JsonProperty(PropertyName = "context")]
        public ContextEventAttribute contextAttribute;

        public string timestamp;
    }
}
