﻿using System;

namespace SchoolGame.Services.Models
{
    public class WordSvcModel : BaseSvcModel
    {
        public string Value { get; set; }
        public string ImageUrl { get; set; }
        public Guid WordDatasetId { get; set; }
    }
}
