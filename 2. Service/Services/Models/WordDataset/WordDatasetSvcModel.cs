﻿
using System;
using System.Collections.Generic;

namespace SchoolGame.Services.Models
{
    public class WordDatasetSvcModel : BaseSvcModel
    {
        public Guid? TeacherId { get; set; }

        public string Name { get; set; }

        public bool IsPublic { get; set; }

        public List<Guid> StudentIds { get; set; }

        public List<WordSvcModel> Words { get; set; }
    }
}
