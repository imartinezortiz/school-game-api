﻿using System;
using System.Collections.Generic;

namespace SchoolGame.Services.Models
{
    public class StudentGameConfigurationSvcModel : BaseSvcModel
    {
        public Guid StudentId { get; set; }
        public StudentSvcModel Student { get; set; }
        public List<StudentGameConfigurationWordDatasetSvcModel> StudentGameConfigurationWordDatasets { get; set; }
        public int Level { get; set; }
        public int SuggestChangeLevelValue { get; set; }
        public int HighScore { get; set; }
    }
}
