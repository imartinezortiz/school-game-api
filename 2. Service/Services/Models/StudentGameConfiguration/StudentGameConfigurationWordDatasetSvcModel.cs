﻿using System;

namespace SchoolGame.Services.Models
{
    public class StudentGameConfigurationWordDatasetSvcModel: BaseSvcModel
    {
        public Guid WordDatasetId { get; set; }
        public WordDatasetSvcModel WordDataset { get; set; }

        public Guid StudentGameConfigurationId { get; set; }
        public StudentGameConfigurationSvcModel StudentGameConfiguration { get; set; }
    }
}
