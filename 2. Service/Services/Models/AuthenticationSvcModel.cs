﻿
namespace SchoolGame.Services.Models
{
    public class AuthenticationSvcModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
