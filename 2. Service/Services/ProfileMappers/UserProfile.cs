﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.Services.Helpers.ProfileMappers
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserSvcModel>()
                .ReverseMap();
        }

    }
}
