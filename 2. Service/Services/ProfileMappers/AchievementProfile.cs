﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.Services.Helpers.ProfileMappers
{
    public class AchievementProfile : Profile
    {
        public AchievementProfile()
        {
            CreateMap<Achievement, AchievementSvcModel>()
                .ReverseMap();
            CreateMap<StudentAchievementSvcModel, StudentAchievement>()
                .ReverseMap();
        }

    }
}
