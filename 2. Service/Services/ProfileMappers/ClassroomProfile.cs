﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using System.Linq;

namespace SchoolGame.Services.Helpers.ProfileMappers
{
    public class ClassroomProfile : Profile
    {
        public ClassroomProfile()
        {
            CreateMap<Classroom, ClassroomSvcModel>()
                .ForMember(dest => dest.TeachersIds, opt => opt.MapFrom(src => src.ClassroomsTeachers.Select(ct => ct.TeacherId).ToList()));
            CreateMap<ClassroomSvcModel, Classroom>();

            CreateMap<ClassroomTeacher, ClassroomTeacherSvcModel>()
                .ReverseMap();
        }

    }
}
