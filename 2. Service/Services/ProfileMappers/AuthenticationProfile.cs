﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.Services.Helpers.ProfileMappers
{
    public class AuthenticationProfile : Profile
    {
        public AuthenticationProfile()
        {
            CreateMap<User, AuthenticationSvcModel>()
                .ForMember(
                    destination => destination.Login,
                    options => options.MapFrom(source => source.Email)
                )
                .ForMember(
                    destination => destination.Password,
                    options => options.MapFrom(source => source.Password)
                );
        }
    }
}
