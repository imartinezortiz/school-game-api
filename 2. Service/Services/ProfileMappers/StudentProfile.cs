﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;
using System.Linq;

namespace SchoolGame.Services.Helpers.ProfileMappers
{
    public class StudentProfile : Profile
    {
        public StudentProfile()
        {
            CreateMap<Student, StudentSvcModel>()
                .ForMember(dest => dest.StudentLearnedWords, opt => opt.MapFrom(src => src.StudentLearnedWords.Select(slw => slw.Word).ToList()));
            CreateMap<StudentSvcModel, Student>()
                .ForMember(dest => dest.StudentLearnedWords, opt => opt.Ignore());
            CreateMap<StudentClassroomSvcModel, StudentClassroom>();

            CreateMap<StudentClassroom, StudentClassroomSvcModel>()
                .ForMember(dest => dest.StudentClassroomStatusCode, opt => opt.MapFrom(src => src.StudentClassroomStatus.Code));
        }

    }
}
