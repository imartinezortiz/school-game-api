﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.Services.Helpers.ProfileMappers
{
    public class GameplayProfile : Profile
    {
        public GameplayProfile()
        {
            CreateMap<Gameplay, GameplaySvcModel>();
            CreateMap<GameplaySvcModel, Gameplay>()
                .ForMember(dest => dest.StudentId, opt => opt.MapFrom(src => src.Student.Id))
                .ForMember(dest => dest.Student, opt => opt.Ignore())
                .ForMember(dest => dest.ObjectiveId, opt => opt.MapFrom(src => src.Objective.Id))
                .ForMember(dest => dest.Objective, opt => opt.Ignore());

            CreateMap<GameplayEvent, GameplayEventSvcModel>()
                .ReverseMap();
        }

    }
}
