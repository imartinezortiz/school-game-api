﻿using AutoMapper;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.Services.Helpers.ProfileMappers
{
    public class StudentGameConfigutationProfile : Profile
    {
        public StudentGameConfigutationProfile()
        {
            CreateMap<StudentGameConfiguration, StudentGameConfigurationSvcModel>()
                .ReverseMap();
            CreateMap<StudentGameConfigurationWordDataset, StudentGameConfigurationWordDatasetSvcModel>()
                .ReverseMap();
            CreateMap<WordDataset, WordDatasetSvcModel>()
                .ReverseMap();
            CreateMap<Word, WordSvcModel>()
                .ReverseMap();

        }

    }
}
