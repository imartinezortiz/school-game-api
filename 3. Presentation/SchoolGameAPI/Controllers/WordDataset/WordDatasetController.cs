﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    [Authorize(Roles = "TEACHER")]
    [Route("api/word-dataset")]
    public class WordDatasetController : ControllerBase
    {
        private readonly ILogger<WordDatasetController> _logger;
        private readonly IMapper mapper;
        private readonly WordDatasetService wordDatasetService;

        public WordDatasetController(ILogger<WordDatasetController> logger, IMapper mapper, WordDatasetService wordDatasetService)
        {
            _logger = logger;
            this.mapper = mapper;
            this.wordDatasetService = wordDatasetService;
        }

        [HttpGet("{wordDatasetId}")]
        public async Task<IActionResult> GetWordDatasetById([FromRoute] Guid wordDatasetId)
        {
            WordDatasetSvcModel wordDatasetSvcModel = await wordDatasetService.GetWordDatasetById(wordDatasetId);

            if (wordDatasetSvcModel == default)
                return NotFound();

            var wordDatasetDto = mapper.Map<WordDatasetDto>(wordDatasetSvcModel);
            return Ok(wordDatasetDto);
        }

        [HttpGet]
        public async Task<IActionResult> GetWordDatasetsByTeacherId([FromQuery] Guid teacherId)
        {
            List<WordDatasetSvcModel> wordDatasetsSvcModel = await wordDatasetService.GetWordDatasetsByTeacherId(teacherId);

            if (wordDatasetsSvcModel == default)
                return NotFound();

            var wordDatasetsDto = mapper.Map<List<WordDatasetDto>>(wordDatasetsSvcModel);
            return Ok(wordDatasetsDto);
        }

        [HttpPost]
        public async Task<IActionResult> PostWordDataset([FromBody] WordDatasetDtoRequest wordDatasetDtoRequest)
        {
            var wordDatasetSvcModel = mapper.Map<WordDatasetSvcModel>(wordDatasetDtoRequest);
            wordDatasetSvcModel = await wordDatasetService.CreateNewWordDataset(wordDatasetSvcModel);

            if (wordDatasetSvcModel == default) return BadRequest();

            var wordDatasetDto = mapper.Map<WordDatasetDto>(wordDatasetSvcModel);
            return Ok(wordDatasetDto);
        }

        [HttpPut("{wordDatasetId}")]
        public async Task<IActionResult> PutWordDataset([FromRoute] Guid wordDatasetId, [FromBody] WordDatasetDtoRequest wordDatasetDtoRequest)
        {
            if (wordDatasetId != wordDatasetDtoRequest.Id) return BadRequest();

            var wordDatasetSvcModel = mapper.Map<WordDatasetSvcModel>(wordDatasetDtoRequest);
            wordDatasetSvcModel = await wordDatasetService.UpdateWordDataset(wordDatasetSvcModel);

            if (wordDatasetSvcModel == default) return BadRequest();

            var wordDatasetDto = mapper.Map<WordDatasetDto>(wordDatasetSvcModel);
            return Ok(wordDatasetDto);
        }

        [HttpDelete("{wordDatasetId}")]
        public async Task<IActionResult> DeleteWordDataset([FromRoute] Guid wordDatasetId)
        {
            await wordDatasetService.SoftDeleteWordDataset(wordDatasetId);

            return NoContent();
        }
    }
}
