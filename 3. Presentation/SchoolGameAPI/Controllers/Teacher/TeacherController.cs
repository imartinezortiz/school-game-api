﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    [Authorize(Roles = "TEACHER")]
    [Route("api/teachers")]
    public class TeacherController : ControllerBase
    {
        private readonly ILogger<TeacherController> _logger;
        private readonly IMapper mapper;
        private readonly TeacherService teacherService;
        private readonly StudentService studentService;

        public TeacherController(
            ILogger<TeacherController> logger,
            IMapper mapper,
            TeacherService teacherService,
            StudentService studentService)
        {
            _logger = logger;
            this.mapper = mapper;
            this.teacherService = teacherService;
            this.studentService = studentService;
        }

        [HttpGet("{teacherId}")]
        public async Task<IActionResult> GetTeacherById([FromRoute] Guid teacherId)
        {
            TeacherSvcModel teacherSvcModel = await teacherService.GetTeacherByTeacherId(teacherId);

            if (teacherSvcModel == default)
                return NotFound();

            var teacherDto = mapper.Map<TeacherDto>(teacherSvcModel);
            return Ok(teacherDto);
        }

        [HttpGet("{teacherId}/students")]
        public async Task<IActionResult> GetStudentsByTeacherId([FromRoute] Guid teacherId)
        {
            List<StudentSvcModel> studentsSvcModel = await studentService.GetStudentsByTeacherId(teacherId);

            if (studentsSvcModel == default)
                return NotFound();

            var studentDto = mapper.Map<List<StudentDto>>(studentsSvcModel);
            return Ok(studentDto);
        }

        [HttpGet("")]
        public async Task<IActionResult> GetTeacherByUserId([FromQuery] Guid userId)
        {
            TeacherSvcModel teacherSvcModel = await teacherService.GetTeacherByUserId(userId);

            if (teacherSvcModel == default)
                return NotFound();

            var teacherDto = mapper.Map<TeacherDto>(teacherSvcModel);
            return Ok(teacherDto);
        }

        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] TeacherDtoRequest teacherDtoRequest)
        {
            var teacherSvcModel = mapper.Map<TeacherSvcModel>(teacherDtoRequest);
            teacherSvcModel = await teacherService.CreateNewTeacher(teacherSvcModel);

            if (teacherSvcModel == default) return BadRequest();

            var teacherDto = mapper.Map<TeacherDto>(teacherSvcModel);
            return Ok(teacherDto);
        }
    }
}
