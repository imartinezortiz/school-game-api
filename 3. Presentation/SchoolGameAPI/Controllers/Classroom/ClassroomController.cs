﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    [Route("api/classrooms")]
    public class ClassroomController : ControllerBase
    {
        private readonly ILogger<ClassroomController> _logger;
        private readonly IMapper mapper;
        private readonly ClassroomService classroomService;

        public ClassroomController(ILogger<ClassroomController> logger, IMapper mapper, ClassroomService classroomService)
        {
            _logger = logger;
            this.mapper = mapper;
            this.classroomService = classroomService;
        }

        [Authorize(Roles = "TEACHER")]
        [HttpGet("")]
        public async Task<IActionResult> GetByTeacherId([FromQuery] Guid teacherId)
        {
            List<ClassroomSvcModel> classroomsSvcModel = await classroomService.GetClassroomsByTeacherId(teacherId);

            var classroomsDto = mapper.Map<List<ClassroomDto>>(classroomsSvcModel);
            return Ok(classroomsDto);
        }

        [Authorize(Roles = "TEACHER,STUDENT")]
        [HttpGet("{classroomId}")]
        public async Task<IActionResult> Get([FromRoute] Guid classroomId)
        {
            ClassroomSvcModel classroomSvcModel = await classroomService.GetClassroomByClassroomId(classroomId);

            var classroomDto = mapper.Map<ClassroomDto>(classroomSvcModel);
            return Ok(classroomDto);
        }

        [Authorize(Roles = "TEACHER")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ClassroomDtoRequest classroomDtoRequest)
        {
            var classroomSvcModel = mapper.Map<ClassroomSvcModel>(classroomDtoRequest);
            classroomSvcModel = await classroomService.CreateNewClassroom(classroomSvcModel);

            if (classroomSvcModel == default) return BadRequest();

            var classroomDto = mapper.Map<ClassroomDto>(classroomSvcModel);
            return Ok(classroomDto);
        }

        [Authorize(Roles = "TEACHER")]
        [HttpPost("generate-code")]
        public IActionResult GenerateCode()
        {
            string code = classroomService.GenerateCode();

            return Ok(new { code });
        }

        [Authorize(Roles = "TEACHER")]
        [HttpPut("{classroomId}")]
        public IActionResult Update([FromRoute] Guid classroomId, [FromBody] ClassroomDtoRequest classroomDtoRequest)
        {
            if (classroomId != classroomDtoRequest.Id) return BadRequest();

            var classroomSvcModel = mapper.Map<ClassroomSvcModel>(classroomDtoRequest);
            classroomSvcModel = classroomService.UpdateClassroom(classroomSvcModel);

            if (classroomSvcModel == default) return BadRequest();

            var classroomDto = mapper.Map<ClassroomDto>(classroomSvcModel);
            return Ok(classroomDto);
        }

        [Authorize(Roles = "TEACHER")]
        [HttpDelete("{classroomId}")]
        public async Task<IActionResult> DeleteClassroom([FromRoute] Guid classroomId)
        {
            await classroomService.DeleteClassroom(classroomId);

            return NoContent();
        }
    }
}
