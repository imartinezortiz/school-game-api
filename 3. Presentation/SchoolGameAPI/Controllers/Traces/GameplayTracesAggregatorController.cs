﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SchoolGame.Services.Services;
using System;
using System.Threading.Tasks;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    //[Authorize(Roles = "TEACHER")]
    [AllowAnonymous]
    [Route("api/gameplay-events/aggregator/{actorId}")]
    public class GameplayTracesAggregatorController : ControllerBase
    {
        private readonly GameplayTracesAggregatorService gameplayTracesAggregatorService;

        public GameplayTracesAggregatorController(GameplayTracesAggregatorService gameplayTracesAggregatorService)
        {
            this.gameplayTracesAggregatorService = gameplayTracesAggregatorService;
        }

        [HttpGet("gameplay-duration")]
        public async Task<IActionResult> GetGameplayDuration([FromRoute] Guid actorId)
        {
            return Ok(await this.gameplayTracesAggregatorService.GetGameplayDuration(actorId));
        }

        [HttpGet("gameplay-statistics")]
        public async Task<IActionResult> GetGameplayStatistics([FromRoute] Guid actorId)
        {
            return Ok(await this.gameplayTracesAggregatorService.GetGameplayStatistics(actorId));
        }

    }
}
