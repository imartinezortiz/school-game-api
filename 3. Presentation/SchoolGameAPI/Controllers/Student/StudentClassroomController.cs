﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    [Authorize(Roles = "STUDENT,TEACHER")]
    [Route("api/students/classroom")]
    public class StudentClassroomController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly StudentClassroomService studentClassroomService;

        public StudentClassroomController(
            IMapper mapper,
            StudentClassroomService studentClassroomService
        )
        {
            this.mapper = mapper;
            this.studentClassroomService = studentClassroomService;
        }

        [HttpGet]
        public async Task<IActionResult> GetStudentClassroomByStudentId([FromQuery] Guid studentId, [FromQuery] Guid teacherId)
        {
            if ((studentId == default && teacherId == default) || (studentId != default && teacherId != default)) return BadRequest();
            
            if(studentId != default)
            {
                StudentClassroomSvcModel studentClassroomSvcModel = await studentClassroomService.GetStudentClassroomByStudentId(studentId);

                if (studentClassroomSvcModel == null) return NotFound();

                var studentClassroomDto = mapper.Map<StudentClassroomDto>(studentClassroomSvcModel);
                return Ok(studentClassroomDto);
            }
            else
            {
                List<StudentClassroomSvcModel> studentClassroomSvcModel = await studentClassroomService.GetStudentClassroomsByTeacherId(teacherId);

                if (studentClassroomSvcModel == null) return NotFound();

                var studentClassroomDto = mapper.Map<List<StudentClassroomDto>>(studentClassroomSvcModel);
                return Ok(studentClassroomDto);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateStudentClassroom([FromBody] StudentClassroomDtoRequest studentClassroomDtoRequest)
        {
            var studentClassroomSvcModel = mapper.Map<StudentClassroomSvcModel>(studentClassroomDtoRequest);
            studentClassroomSvcModel = await studentClassroomService.CreateNewStudentClassroom(studentClassroomSvcModel, studentClassroomDtoRequest.Code);

            if (studentClassroomSvcModel == default) return BadRequest();

            var studentClassroomDto = mapper.Map<StudentClassroomDto>(studentClassroomSvcModel);
            return Ok(studentClassroomDto);
        }

        [HttpPut("{studentClassroomId}")]
        public async Task<IActionResult> UpdateStudentClassroomMessage([Required, FromRoute] Guid studentClassroomId, [FromBody] StudentClassroomDtoRequest studentClassroomDtoRequest)
        {
            if (studentClassroomId != studentClassroomDtoRequest.Id) return BadRequest();

            var studentClassroomSvcModel = mapper.Map<StudentClassroomSvcModel>(studentClassroomDtoRequest);
            studentClassroomSvcModel = await studentClassroomService.UpdateStudentClassroom(studentClassroomSvcModel);

            if (studentClassroomSvcModel == null) return BadRequest();

            var studentClassroomDto = mapper.Map<StudentClassroomDto>(studentClassroomSvcModel);
            return Ok(studentClassroomDto);
        }

        [HttpDelete("{studentClassroomId}")]
        public async Task<IActionResult> DeleteStudentClassroom([Required, FromRoute] Guid studentClassroomId)
        {
            bool isDeleted = await studentClassroomService.DeleteStudentClassroom(studentClassroomId);

            if (!isDeleted) return NotFound();

            return NoContent();
        }
    }
}
