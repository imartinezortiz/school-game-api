﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services;
using System;
using System.Threading.Tasks;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    [Route("api/students")]
    public class StudentController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly StudentService studentService;

        public StudentController(IMapper mapper, StudentService studentService)
        {
            this.mapper = mapper;
            this.studentService = studentService;
        }

        [Authorize(Roles = "STUDENT")]
        [HttpGet("{studentId}")]
        public async Task<IActionResult> GetStudent([FromRoute] Guid studentId)
        {
            StudentSvcModel studentSvcModel = await studentService.GetStudentById(studentId);

            if (studentSvcModel == default)
                return NotFound();

            var studentDto = mapper.Map<StudentDto>(studentSvcModel);
            return Ok(studentDto);
        }

        [Authorize(Roles = "TEACHER,STUDENT")]
        [HttpGet("")]
        public async Task<IActionResult> GetStudentByUserId([FromQuery] Guid userId)
        {
            StudentSvcModel studentSvcModel = await studentService.GetStudentByUserId(userId);

            if (studentSvcModel == default)
                return NotFound();

            var studentDto = mapper.Map<StudentDto>(studentSvcModel);
            return Ok(studentDto);
        }

        [HttpPost]
        public async Task<IActionResult> CreateStudent([FromBody] StudentDtoRequest studentDtoRequest)
        {
            var studentSvcModel = mapper.Map<StudentSvcModel>(studentDtoRequest);
            studentSvcModel = await studentService.CreateNewStudent(studentSvcModel);

            if (studentSvcModel == default) return BadRequest();

            var studentDto = mapper.Map<StudentDto>(studentSvcModel);
            return Ok(studentDto);
        }

        [Authorize(Roles = "STUDENT")]
        [HttpDelete("{studentId}")]
        public async Task<IActionResult> DeleteStudent([FromRoute] Guid studentId)
        {
            if (await this.studentService.DeleteStudentById(studentId))
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
