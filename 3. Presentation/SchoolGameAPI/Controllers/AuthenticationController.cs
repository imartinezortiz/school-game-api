﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    [Route("api/authentication")]
    public class AuthenticationController : ControllerBase
    {
        private readonly ILogger<AuthenticationController> _logger;
        private readonly IMapper mapper;
        private readonly SchoolGameAuthenticationService authenticationService;
        private readonly UserService userService;

        public AuthenticationController(
            ILogger<AuthenticationController> logger,
            IMapper mapper,
            SchoolGameAuthenticationService authenticationService,
            UserService userService)
        {
            _logger = logger;
            this.mapper = mapper;
            this.authenticationService = authenticationService;
            this.userService = userService;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetUserAuthenticated()
        {
            var userId = Guid.Parse(User.Claims.Where(c => c.Type.Equals(ClaimTypes.NameIdentifier)).FirstOrDefault().Value);

            UserSvcModel userSvcModel = await userService.GetUserByUserId(userId);
            var user = mapper.Map<UserDto>(userSvcModel);
            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticationRequestDto authentication)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var authenticationSvcModel = mapper.Map<AuthenticationSvcModel>(authentication);

            authenticationSvcModel = await authenticationService.AuthenticateUser(authenticationSvcModel);

            if (authenticationSvcModel == null || authenticationSvcModel.Token == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            var response = mapper.Map<AuthenticationResponseDto>(authenticationSvcModel);

            return Ok(response);
        }
    }
}
