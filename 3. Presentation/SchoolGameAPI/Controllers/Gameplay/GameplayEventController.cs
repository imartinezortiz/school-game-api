﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    [Authorize(Roles = "STUDENT")]
    [Route("api/gameplays/{gameplayId}/events")]
    public class GameplayEventController : ControllerBase
    {
        private readonly ILogger<AuthenticationController> _logger;
        private readonly IMapper mapper;
        private readonly GameplayEventService gameplayEventService;

        public GameplayEventController(ILogger<AuthenticationController> logger,
            IMapper mapper,
            GameplayEventService gameplayEventService)
        {
            _logger = logger;
            this.mapper = mapper;
            this.gameplayEventService = gameplayEventService;
        }

        /*[HttpGet("{gameplayEventId}")]
        public async Task<IActionResult> GetGameplay([FromRoute] Guid gameplayId)
        {
            GameplaySvcModel gameplaySvcModel = await gameplayEventService.GetGameplayByGameplayId(gameplayId);

            var gameplayDto = mapper.Map<GameplayDto>(gameplaySvcModel);
            return Ok(gameplayDto);
        }*/

        [HttpGet]
        public async Task<IActionResult> GetGameplay([FromRoute] Guid gameplayId)
        {
            List<GameplayEventSvcModel> gameplaySvcModel = await gameplayEventService.GetGameplayEventsByGameplayId(gameplayId);

            var gameplayEventsDto = mapper.Map<List<GameplayEventDto>>(gameplaySvcModel);
            //return Ok(gameplayDto);
            //return Ok(gameplayDto.Select(x => x.JsonEvent).ToList());
            //var jsonEvents = gameplayEventsDto.ConvertAll(x => JsonSerializer.Deserialize(x.JsonEvent, typeof(dynamic)));
            List<Event> jsonEvents = gameplayEventsDto.ConvertAll(x => JsonConvert.DeserializeObject<Event>(x.JsonEvent));
            jsonEvents.ForEach(x => x.timestamp = DateTimeOffset.Parse(x.timestamp).ToString("yyyy-MM-ddTHH:mm:ssZ"));
            var a = jsonEvents.ConvertAll(x => JsonConvert.SerializeObject(x));
            return Ok(a.ConvertAll(x => System.Text.Json.JsonSerializer.Deserialize(x, typeof(object))));
        }

        [HttpPost]
        public async Task<IActionResult> PostGameplayEvent([FromRoute] Guid gameplayId, [FromBody] GameplayEventDto gameplayEventDtoRequest)
        {
            var gameplayEventSvcModel = mapper.Map<GameplayEventSvcModel>(gameplayEventDtoRequest);
            gameplayEventSvcModel.GameplayId = gameplayId;

            gameplayEventSvcModel = await gameplayEventService.CreateNewGameplayEvent(gameplayEventSvcModel);

            var gameplayEventDto = mapper.Map<GameplayEventDto>(gameplayEventSvcModel);
            return Ok(gameplayEventDto);
        }

    }
}
