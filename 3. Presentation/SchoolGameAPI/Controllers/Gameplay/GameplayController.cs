﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    [Authorize(Roles = "STUDENT")]
    [Route("api/gameplays")]
    public class GameplayController : ControllerBase
    {
        private readonly ILogger<AuthenticationController> _logger;
        private readonly IMapper mapper;
        private readonly GameplayService gameplayService;
        private readonly StudentService studentService;

        public GameplayController(ILogger<AuthenticationController> logger,
            IMapper mapper,
            GameplayService gameplayService,
            StudentService studentService)
        {
            _logger = logger;
            this.mapper = mapper;
            this.gameplayService = gameplayService;
            this.studentService = studentService;
        }

        [HttpGet("{gameplayId}")]
        public async Task<IActionResult> GetGameplay([FromRoute] Guid gameplayId)
        {
            GameplaySvcModel gameplaySvcModel = await gameplayService.GetGameplayByGameplayId(gameplayId);

            var gameplayDto = mapper.Map<GameplayDto>(gameplaySvcModel);
            return Ok(gameplayDto);
        }

        [HttpPost]
        public async Task<IActionResult> PostGameplay([FromBody] GameplayDto gameplayDtoRequest)
        {
            var userId = Guid.Parse(User.Claims.Where(c => c.Type.Equals(ClaimTypes.NameIdentifier)).FirstOrDefault().Value);

            var gameplaySvcModel = mapper.Map<GameplaySvcModel>(gameplayDtoRequest);

            gameplaySvcModel.Student = await studentService.GetStudentByUserId(userId);

            gameplaySvcModel = await gameplayService.CreateNewGameplay(gameplaySvcModel);

            var gameplayDto = mapper.Map<GameplayDto>(gameplaySvcModel);
            return Ok(gameplayDto);
        }

        [HttpPut("{gameplayId}")]
        public IActionResult PutGameplay(
            [FromRoute, Required] Guid gameplayId,
            [FromBody] GameplayDto gameplayDtoRequest)
        {
            if (!gameplayId.Equals(gameplayDtoRequest.Id))
            {
                return BadRequest();
            }

            Guid userId = Guid.Parse(User.Claims.Where(c => c.Type.Equals(ClaimTypes.NameIdentifier)).FirstOrDefault().Value);
            if (!userId.Equals(gameplayDtoRequest.UserId))
            {
                return NotFound();
            }

            var gameplaySvcModel = mapper.Map<GameplaySvcModel>(gameplayDtoRequest);

            gameplaySvcModel = gameplayService.UpdateGameplay(gameplaySvcModel);

            var gameplayDto = mapper.Map<GameplayDto>(gameplaySvcModel);
            return Ok(gameplayDto);
        }
    }
}
