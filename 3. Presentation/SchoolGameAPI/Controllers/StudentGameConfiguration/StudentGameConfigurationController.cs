﻿using Microsoft.AspNetCore.Mvc;
using System;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services;
using AutoMapper;
using System.Threading.Tasks;
using SchoolGame.SchoolGameAPI.Models;
using System.Text.Json;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    [Route("api/student-game-configurations")]
    public class StudentGameConfigurationController : Controller
    {
        private readonly IMapper mapper;
        private readonly StudentGameConfigurationService studentGameConfigurationService;
        private readonly StudentService studentService;

        public StudentGameConfigurationController(
            IMapper mapper,
            StudentGameConfigurationService studentGameConfigurationService,
            StudentService studentService
        ) {
            this.mapper = mapper;
            this.studentGameConfigurationService = studentGameConfigurationService;
            this.studentService = studentService;
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetStudentGameConfigurationByUserId(Guid userId)
        {
            Guid studentId = (await studentService.GetStudentByUserId(userId)).Id;

            StudentGameConfigurationSvcModel studentGameConfigurationSvcModel = await studentGameConfigurationService.GetStudentGameConfigurationByStudentId(studentId);
            return Ok(JsonSerializer.Serialize(mapper.Map<StudentGameConfigurationDto>(studentGameConfigurationSvcModel)));
        }

        [HttpPut("{studentGameConfigurationId}")]
        public async Task<IActionResult> UpdateStudentGameConfiguration([FromRoute] Guid studentGameConfigurationId, [FromBody] StudentGameConfigurationDtoRequest studentGameConfigurationDto)
        {
            if (studentGameConfigurationId != studentGameConfigurationDto.Id)
            {
                return BadRequest();
            }

            var studentGameConfigurationSvcModel = mapper.Map<StudentGameConfigurationSvcModel>(studentGameConfigurationDto);

            await studentGameConfigurationService.UpdateStudentGameConfiguration(studentGameConfigurationSvcModel);

            return Ok();
        }
    }
}
