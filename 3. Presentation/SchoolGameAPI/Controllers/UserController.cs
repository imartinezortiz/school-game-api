﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;
using SchoolGame.Services.Services;
using System;
using System.Threading.Tasks;

namespace SchoolGame.SchoolGameAPI.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<AuthenticationController> _logger;
        private readonly IMapper mapper;
        private readonly UserService userService;

        public UserController(ILogger<AuthenticationController> logger,
            IMapper mapper,
            UserService userService)
        {
            _logger = logger;
            this.mapper = mapper;
            this.userService = userService;
        }

        [Authorize]
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUser([FromRoute] Guid userId)
        {
            UserSvcModel userSvcModel = await userService.GetUserByUserId(userId);

            var userDto = mapper.Map<UserDto>(userSvcModel);
            return Ok(userDto);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] UserDtoRequest userDtoRequest)
        {
            if (userDtoRequest.Role != "TEACHER" && userDtoRequest.Role != "STUDENT")
            {
                return BadRequest();
            }

            var userSvcModel = mapper.Map<UserSvcModel>(userDtoRequest);
            userSvcModel = await userService.CreateNewUser(userSvcModel, userDtoRequest.Name);

            var userDto = mapper.Map<UserDto>(userSvcModel);
            return Ok(userDto);
        }
    }
}
