﻿using AutoMapper;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;
using System.Linq;

namespace SchoolGame.SchoolGameAPI.Helpers.ProfileMappers
{
    public class StudentGameConfigutationProfile : Profile
    {
        public StudentGameConfigutationProfile()
        {
            CreateMap<StudentGameConfigurationDto, StudentGameConfigurationSvcModel>()
                .ReverseMap();
            CreateMap<StudentGameConfigurationDtoRequest, StudentGameConfigurationSvcModel>()
                .ForMember(
                    dest => dest.StudentGameConfigurationWordDatasets,
                    opt => opt.MapFrom(
                        src => src.WordDatasetIds.Select(x => new StudentGameConfigurationWordDatasetSvcModel(){
                            StudentGameConfigurationId = src.Id,
                            WordDatasetId = x
                        }).ToList()));
            CreateMap<StudentGameConfigurationWordDatasetDto, StudentGameConfigurationWordDatasetSvcModel>()
                .ReverseMap();
        }

    }
}
