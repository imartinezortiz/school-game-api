﻿using AutoMapper;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.SchoolGameAPI.Helpers.ProfileMappers
{
    public class GameplayProfile : Profile
    {
        public GameplayProfile()
        {
            CreateMap<GameplayDto, GameplaySvcModel>()
                .ReverseMap();
            CreateMap<GameplayEventDto, GameplayEventSvcModel>()
                .ReverseMap();
        }

    }
}
