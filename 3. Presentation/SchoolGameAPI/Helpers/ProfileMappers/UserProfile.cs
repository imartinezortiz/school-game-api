﻿using AutoMapper;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.SchoolGameAPI.Helpers.ProfileMappers
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, UserSvcModel>()
                .ReverseMap();
            CreateMap<UserDtoRequest, UserSvcModel>()
                .ReverseMap();
        }

    }
}
