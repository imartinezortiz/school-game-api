﻿using AutoMapper;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.SchoolGameAPI.Helpers.ProfileMappers
{
    public class WordDatasetProfile : Profile
    {
        public WordDatasetProfile()
        {
            CreateMap<WordDatasetDto, WordDatasetSvcModel>()
                .ReverseMap();
            CreateMap<WordDto, WordSvcModel>()
                .ReverseMap();
            CreateMap<WordDatasetDtoRequest, WordDatasetSvcModel>()
                .ReverseMap();
        }

    }
}
