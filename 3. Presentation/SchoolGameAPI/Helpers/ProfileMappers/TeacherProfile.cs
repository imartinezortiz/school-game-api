﻿using AutoMapper;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.SchoolGameAPI.Helpers.ProfileMappers
{
    public class TeacherProfile : Profile
    {
        public TeacherProfile()
        {
            CreateMap<TeacherDto, TeacherSvcModel>()
                .ReverseMap();
            CreateMap<TeacherDtoRequest, TeacherSvcModel>()
                .ReverseMap();
        }

    }
}
