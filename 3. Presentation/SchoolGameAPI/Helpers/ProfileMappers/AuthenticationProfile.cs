﻿using AutoMapper;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Entities.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.SchoolGameAPI.Helpers.ProfileMappers
{
    public class AuthenticationProfile : Profile
    {
        public AuthenticationProfile()
        {
            CreateMap<AuthenticationRequestDto, AuthenticationSvcModel>()
                .ForMember(
                    destination => destination.Login,
                    options => options.MapFrom(source => source.Email)
                )
                .ForMember(
                    destination => destination.Password,
                    options => options.MapFrom(source => source.Password)
                );
            CreateMap<AuthenticationSvcModel, AuthenticationResponseDto>();
        }
    }
}
