﻿using AutoMapper;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.SchoolGameAPI.Helpers.ProfileMappers
{
    public class ClassroomProfile : Profile
    {
        public ClassroomProfile()
        {
            CreateMap<ClassroomDto, ClassroomSvcModel>().ReverseMap();

            CreateMap<ClassroomDtoRequest, ClassroomSvcModel>().ReverseMap();
        }

    }
}
