﻿using AutoMapper;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.SchoolGameAPI.Helpers.ProfileMappers
{
    public class StudentProfile : Profile
    {
        public StudentProfile()
        {
            CreateMap<StudentDto, StudentSvcModel>()
                .ReverseMap();
            CreateMap<StudentDtoRequest, StudentSvcModel>()
                .ReverseMap();
            CreateMap<StudentClassroomDto, StudentClassroomSvcModel>()
                .ReverseMap();
            CreateMap<StudentClassroomDtoRequest, StudentClassroomSvcModel>()
                .ReverseMap();
        }

    }
}
