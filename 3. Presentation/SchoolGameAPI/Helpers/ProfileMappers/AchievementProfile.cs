﻿using AutoMapper;
using SchoolGame.SchoolGameAPI.Models;
using SchoolGame.Services.Models;

namespace SchoolGame.SchoolGameAPI.Helpers.ProfileMappers
{
    public class AchievementProfile : Profile
    {
        public AchievementProfile()
        {
            CreateMap<StudentAchievementDto, StudentAchievementSvcModel>()
                .ReverseMap();
            CreateMap<AchievementDto, AchievementSvcModel>()
                .ReverseMap();
        }

    }
}
