﻿namespace SchoolGame.SchoolGameAPI.Models
{
    public class GameplayEventDto
    {
        public string JsonEvent { get; set; }
    }
}
