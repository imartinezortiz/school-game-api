﻿using System;
using System.Collections.Generic;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class GameplayDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }

        public WordDto Objective { get; set; }

        public DateTimeOffset StartDate { get; set; } = DateTimeOffset.Now;

        public DateTimeOffset? EndDate { get; set; }

        public int Level { get; set; } = 0;

        public int Score { get; set; } = 0;

        public bool IsObjectiveAchieved { get; set; } = false;

        public List<GameplayEventDto> GameplayEvents { get; set; }
    }
}
