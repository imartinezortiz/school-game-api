﻿using System;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class ClassroomDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SchoolName { get; set; }
        public string SchoolGrade { get; set; }
        public string Code { get; set; }
    }
}
