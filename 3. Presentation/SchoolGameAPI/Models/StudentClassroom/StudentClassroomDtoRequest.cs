﻿using System;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class StudentClassroomDtoRequest
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public int StudentClassroomStatusCode { get; set; }
        public string Message { get; set; }
        public Guid StudentId { get; set; }
    }
}
