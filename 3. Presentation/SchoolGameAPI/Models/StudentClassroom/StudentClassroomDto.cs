﻿using System;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class StudentClassroomDto
    {
        public Guid Id { get; set; }
        public int StudentClassroomStatusCode { get; set; }
        public Guid ClassroomId { get; set; }
        public ClassroomDto Classroom { get; set; }
        public Guid StudentId { get; set; }
        public StudentDto Student { get; set; }
        public string Message { get; set; }
    }
}
