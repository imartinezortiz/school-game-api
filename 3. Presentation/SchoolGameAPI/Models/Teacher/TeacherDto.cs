﻿using System;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class TeacherDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public UserDto User { get; set; }
        public string Name { get; set; }
    }
}
