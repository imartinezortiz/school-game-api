﻿using System;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class TeacherDtoRequest
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
    }
}
