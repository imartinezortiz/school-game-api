﻿using System;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class StudentAchievementDto
    {
        public Guid Id { get; set; }
        public Guid StudentId { get; set; }

        public Guid AchievementId { get; set; }
        public virtual AchievementDto Achievement { get; set; }

        public int CurrentValue { get; set; }
    }
}
