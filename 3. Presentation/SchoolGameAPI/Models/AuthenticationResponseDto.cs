﻿
namespace SchoolGame.SchoolGameAPI.Models
{
    public class AuthenticationResponseDto
    {
        public string Token { get; set; }
    }
}
