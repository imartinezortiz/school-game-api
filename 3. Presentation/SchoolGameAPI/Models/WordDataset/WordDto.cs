﻿using System;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class WordDto
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public string ImageUrl { get; set; }
        public Guid WordDatasetId { get; set; }
    }
}
