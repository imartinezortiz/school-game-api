﻿using System;
using System.Collections.Generic;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class WordDatasetDto
    {
        public Guid Id { get; set; }
        public Guid? TeacherId { get; set; }

        public string Name { get; set; }

        public bool IsPublic { get; set; }

        public List<Guid> StudentIds { get; set; }

        public List<WordDto> Words { get; set; }
    }
}
