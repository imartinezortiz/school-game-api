﻿using System;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class StudentDtoRequest
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid? TeacherId { get; set; }
    }
}
