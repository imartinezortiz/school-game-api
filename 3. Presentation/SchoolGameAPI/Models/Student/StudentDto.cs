﻿using System;
using System.Collections.Generic;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class StudentDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public UserDto User { get; set; }
        public Guid? TeacherId { get; set; }
        public TeacherDto Teacher { get; set; }
        public string Name { get; set; }
        public List<StudentAchievementDto> StudentAchievements { get; set; }
        public List<WordDto> StudentLearnedWords { get; set; }
    }
}
