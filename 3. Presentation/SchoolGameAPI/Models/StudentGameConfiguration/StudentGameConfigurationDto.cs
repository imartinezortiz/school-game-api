﻿using System;
using System.Collections.Generic;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class StudentGameConfigurationDto
    {
        public Guid Id { get; set; }
        public Guid StudentId { get; set; }
        public StudentDto Student { get; set; }
        public List<StudentGameConfigurationWordDatasetDto> StudentGameConfigurationWordDatasets { get; set; }
        public List<WordDto> StudentLearnedWords { get; set; }
        public int Level { get; set; }
        public int SuggestChangeLevelValue { get; set; }
        public int HighScore { get; set; }
    }
}
