﻿using System;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class StudentGameConfigurationWordDatasetDto
    {
        public Guid WordDatasetId { get; set; }
        public WordDatasetDto WordDataset { get; set; }

        public Guid StudentGameConfigurationId { get; set; }
        //public StudentGameConfigurationDto StudentGameConfiguration { get; set; }
    }
}
