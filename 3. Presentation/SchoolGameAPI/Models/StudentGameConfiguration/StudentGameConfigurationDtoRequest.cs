﻿using System;
using System.Collections.Generic;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class StudentGameConfigurationDtoRequest
    {
        public Guid Id { get; set; }
        public Guid StudentId { get; set; }
        public List<Guid> WordDatasetIds { get; set; }
        public List<WordDto> StudentLearnedWords { get; set; }
    }
}
