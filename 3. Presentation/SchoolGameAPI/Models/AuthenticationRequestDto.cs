﻿
using System.ComponentModel.DataAnnotations;

namespace SchoolGame.SchoolGameAPI.Models
{
    public class AuthenticationRequestDto
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
