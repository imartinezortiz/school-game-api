using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using SchoolGame.Entities;
using SchoolGame.Services.Services;
using SchoolGame.Services.Services.Achievements;
using SchoolGame.UnitOfWork;
using System.Reflection;
using System.Text;

namespace SchoolGame.SchoolGameAPI
{
    public class Startup
    {

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var assembliesWithMappings = new[]
            {
                Assembly.Load("SchoolGameAPI"),
                Assembly.Load("Services")
            };

            services.AddAutoMapper(assembliesWithMappings);

            services.AddDbContext<SchoolGameContext>();
            services.AddScoped<SchoolGameContext>();
            services.AddScoped<IUnitOfWork, UnitOfWorkImplementation>();

            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.WithOrigins(Configuration.GetSection("CORS:AllowedOrigins").Get<string[]>());
                        builder.AllowAnyMethod();
                        builder.AllowAnyHeader();
                        builder.AllowCredentials();
                    });
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Secret"]))
                    };
                    options.SaveToken = true;
                });

            services.AddControllers();

            services.AddScoped<SchoolGameAuthenticationService>();
            services.AddScoped<UserService>();
            services.AddScoped<StudentService>();
            services.AddScoped<TeacherService>();
            services.AddScoped<ClassroomService>();
            services.AddScoped<StudentClassroomService>();
            services.AddScoped<GameplayService>();
            services.AddScoped<StudentGameConfigurationService>();
            services.AddScoped<AchievementFactory>();
            services.AddScoped<WordDatasetService>();
            services.AddScoped<GameplayEventService>();

            services.AddScoped<GameplayTracesAggregatorService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            /*if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }*/
            app.UseDeveloperExceptionPage();

            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("Referrer-Policy", "strict-origin-when-cross-origin");
                await next();
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers()
                    .RequireCors(MyAllowSpecificOrigins);
            });
        }
    }
}
